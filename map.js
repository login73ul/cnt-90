function Map(node, width, height) {
    this.controls = [];
    this.node = node;
    this.paper = Raphael(node.get(0), width, height);
    this.listener = {};
    this.last_state = null;
    this.send_to_state = null;

    this.addListener('state_change', this, this.stateChange);
    this.addListener('fatal', this, this.stateChangeFail);
    //this.addListener('warning', this, this.stateChangeWarning);
    this.mode = deviceMode.EMULATOR;
    this.actionManager = new ActionManager(this);
    this.action_depth = 0;
    this.exercises = [];
    this.startstate = false;
    this.addListener('control_action', this, function (source, params) {
        var control = params.control;
        console.log(control.name, control.box());
    })
}



Map.prototype.setMode = function (mode) {
    this.mode = mode;
    try {
        if (mode == deviceMode.HELP) {		// включение подсказок в режиме справки. Троицкий - выравнивать аcпирантов не учат
            $('div#menu').hide();
            $(document).tooltip({ position: { at: "left top", my: "right bottom" } });
            $(document).tooltip("enable");
        } else if (mode == deviceMode.TRAINING) {
            $('div#menu').show();
            $(document).tooltip("disable");
        } else if (mode == deviceMode.EMULATOR) {
            $('div#menu').hide();
            $(document).tooltip("disable");
        } else {
            $('div#menu').show();
            $(document).tooltip("disable");
        }
    } catch (e) { }
    this.draw();
}

Map.prototype.start = function () {
    this.startstate = true;
    this.action(this, 'start');
}

Map.prototype.stop = function () {
    this.startstate = false;
    this.action(this, 'stop');
}

Map.prototype.restart = function () {
    this.stop();
    this.state = jQuery.extend(true, {}, this.start_state);
    this.start();
    this.draw();
}

Map.prototype.restartNewState = function (state) {
    this.stop();
    this.state = jQuery.extend(true, {}, state);
    this.start();
    this.draw();
}

Map.prototype.add = function (c) {
    this.controls.push(c);
    c.setMap(this);
}

Map.prototype.stateChange = function () {
    console.log(this.state);
}

Map.prototype.stateChangeFail = function (control, param) {
    alert('Прибор сломался: ' + param);
    this.rollback();
}

Map.prototype.stateChangeWarning = function (control, param) {
    alert(param);
}

Map.prototype.trystateback = function () {
    if (this.action_depth == 0 && this.send_to_state != null) {
        this.state = this.send_to_state;
        this.send_to_state = null;
        this.action(this, "state_change");
    }
}

Map.prototype.rollback = function () {
    this.send_to_state = this.last_state;
    this.trystateback();
}

Map.prototype.rollbackTo = function (preview_state) { 				// Откат к укзанному состоянию. Троицкий
    this.send_to_state = preview_state;
    this.trystateback();
}

Map.prototype.control = function (name) {
    var map = this;
    for (var i = 0; i < this.controls.length; i++) {
        var value = this.controls[i];
        if (value.name == name)
            return value;
    }
    return null;
}

Map.prototype.draw = function (c) {
    var map = this;
    $.each(this.controls, function (index, value) {
        if (value.haveArea()) {
            if (map.mode == deviceMode.HELP/* || map.mode == deviceMode.TRAINING*/)		//Заменил. Стало много режимов. Отрисовывать синие квадраты надо только в режиме HELP (бывший CONTROLS). Троицкий
                value.drawControl();
            else
                value.draw();
            // switch(map.mode)
            // {
            // 	case deviceMode.EMULATOR:
            // 		value.draw();
            // 	break;
            // 	case deviceMode.HELP:
            // 		value.drawControl();
            // 	break;
            // 	case deviceMode.TRAINING:
            // 		value.draw();
            // 	break;
            // }
        }
    });
    this.action(this, "state_change");
}

Map.prototype.action = function (control, name, params) {
    var m = this.listener[name];
    if (m === undefined) {
        return;
    }
    this.action_depth++;
    for (var i = 0; i < m.length; i++) {
        d = m[i];
        d.func.call(d.obj, control, params);
    }
    this.action_depth--;

    if (this.action_depth == 0 && name == 'state_change') {
        this.last_state = this.state;
    }
    this.trystateback();
}

Map.prototype.addListener = function (name, object, func) {
    var m = this.listener[name];
    if (m === undefined) {
        m = [];
        this.listener[name] = m;
    }
    m.push({ func: func, obj: object });
}

Map.prototype.state_save = function () {
    this.last_state = this.state;
}

Map.prototype.renderAreaControl = function () {
    var a = this.getArea();
    var c = this.definitionControl();

    m_a = {}
    for (var i = 0; i < a.length; i++) {
        var area = a[i];
        m_a[area.key] = area;
    }

    for (var i = 0; i < c.length; i++) {
        var controldef = c[i];
        var control = new controldef.cls(controldef.key, controldef.param);
        if (control.haveArea()) {
            var area = m_a[controldef.key];
            if (area === undefined) {
                //console.log(controldef);
                throw new Error("not found area: " + controldef.key);
            }
            var render = new Render(area, control);
            control.setRender(render);
        }
        this.add(control);
    }
}

Map.prototype.logControl = function () {
    var a = this.getArea();

    m_a = {}
    for (var i = 0; i < a.length; i++) {
        var area = a[i];
        m_a[area.key] = area;
    }

    var s = ""
    for (var i = 0; i < this.controls.length; i++) {
        var control = this.controls[i];
        var t = "";
        if (m_a[control.name] != undefined)
            t = m_a[control.name].tooltip;
        s += control.name + "\t" + control.constructor.name + "\t" + t + "\n";
    }
    console.log(s);
}

Map.prototype.GetControlByName = function (name) {		// функция для получения доступа к компонентам map. Троицкий
    var i = 0;
    for (; i < this.controls.length; i++) {
        if (this.controls[i].name == name)
            return this.controls[i];
    }
}

Map.prototype.transferSystem = function (value) {
  if (cnt90.state.meas_ment != "") {
    if ((value >= 0.000000001 && value < 0.000001) || (value <= -0.000000001 && value > -0.000001)) {
        return value * 1000000000 + " n" + cnt90.state.meas_ment;
    } else if ((value >= 0.000001 && value < 0.001) || (value <= -0.000001 && value > -0.001)) {
        return value * 1000000 + " mk" + cnt90.state.meas_ment;
    } else if ((value >= 0.001 && value < 1) || (value <= -0.001 && value > -1)) {
        return value * 1000 + " m" + cnt90.state.meas_ment;
    } else if ((value >= 1 && value < 1000) || value == 0 || (value <= -1 && value > -1000)) {
        return value + " " + cnt90.state.meas_ment;
    } else if ((value >= 1000 && value < 1000000) || (value <= -1000 && value > -1000000)) {
        return value / 1000 + " k" + cnt90.state.meas_ment;
    } else if ((value >= 1000000 && value < 1000000000) || (value <= -1000000 && value > -1000000000)) {
        return value / 1000000 + " M" + cnt90.state.meas_ment;
    } else if (value >= 1000000000 || value <= -1000000000) {
        return value / 1000000000 + " G" + cnt90.state.meas_ment;
    }
  } 
  return value;
}

Map.prototype.SetStatPlotParam = function (value) {
    this.stat_mean(value);
    this.stat_max(value);
    this.stat_min(value);
    this.stat_p_p();
    this.stat_adev(value);
    this.stat_std(value);
    cnt90.state.stat_plot.N = cnt90.state.stat_plot.N + 1;
    //this.control("stat_plot_p_p").param.value;
}
Map.prototype.stat_mean = function (freq) {
    cnt90.state.stat_plot.SumFreq = cnt90.state.stat_plot.SumFreq + freq;
    this.mean = cnt90.state.stat_plot.SumFreq / cnt90.state.stat_plot.N;
    cnt90.state.stat_plot.M = this.mean;

}
Map.prototype.stat_min = function (value) {
    if (cnt90.state.stat_plot.Min == undefined || cnt90.state.stat_plot.MinPM == undefined || value < cnt90.state.stat_plot.Min) {
        cnt90.state.stat_plot.Min = value;
        cnt90.state.stat_plot.MinPM = this.transferSystem(value);
    }
}
Map.prototype.stat_max = function (value) {
    if (cnt90.state.stat_plot.Max == undefined || value > cnt90.state.stat_plot.Max) {
        cnt90.state.stat_plot.Max = value;
        cnt90.state.stat_plot.MaxPM = this.transferSystem(value);
    }
}
Map.prototype.stat_p_p = function () {

    cnt90.state.stat_plot.P_P = cnt90.state.stat_plot.Max - cnt90.state.stat_plot.Min;
    cnt90.state.stat_plot.P_PPM = this.transferSystem(cnt90.state.stat_plot.P_P);

}
Map.prototype.stat_adev = function (freq) {
    if (cnt90.state.stat_plot.N == 1) {
        cnt90.state.stat_plot.PrevFreq = freq;
        //break;
    } else {
        x = freq - cnt90.state.stat_plot.PrevFreq;
        this.adev = cnt90.state.stat_plot.Adev + Math.pow(x, 2);
        this.adev = this.adev / 2 * (cnt90.state.stat_plot.N - 1);
        this.adev = Math.sqrt(this.adev);
        cnt90.state.stat_plot.AdevPM = this.transferSystem(this.adev) + "²";
        cnt90.state.stat_plot.PrevFreq = freq;

    }

}
Map.prototype.stat_std = function (freq) {
    if (cnt90.state.stat_plot.N != 1) {
        x = freq - cnt90.state.stat_plot.M;
        this.std = cnt90.state.stat_plot.Std + Math.pow(x, 2);
        this.std = this.std / (cnt90.state.stat_plot.N - 1);
        this.std = Math.sqrt(this.std);
        cnt90.state.stat_plot.Std = this.std;
        cnt90.state.stat_plot.StdPM = this.transferSystem(this.std);
    }
}

///---------для упражнений
Map.prototype.GetExList = function () {				// получить список упражнений для данного map. Троицкий
    var foo = [];
    for (var i = 0; i < this.exercises.length; i++) {
        foo.push(this.exercises[i].name);
    }
    return foo;
}

Map.prototype.GetEx = function (num) {				// получить упражнение по номеру. Троицкий
    return this.exercises[num];
}

Map.prototype.AddAdditionalMenuItem = function (id, header, content) {
    $('div#menu').append("<h3 id=" + id + ">" + header + "</h3>");
    $('div#menu').append("<div id=" + id + ">" + content + "</div>");
    this.additional_menu_items_count++;
    //$('div#menu').accordion("refresh");
}

Map.prototype.RemoveAdditionalMenuItems = function () {
    var i = 0;
    for (; i < this.additional_menu_items_count; i++) {
        $('div#menu h3:last').remove();
        $('div#menu div:last').remove();
    }
    this.additional_menu_items_count = 0;
}

Map.prototype.RemoveAdditionalMenuLast = function () {
    if (this.additional_menu_items_count <= 1) {
        return;
    }
    $('div#menu h3:last').remove();
    $('div#menu div:last').remove();
    this.additional_menu_items_count--;
}

Map.prototype.FocusOnMenuItem = function (num) {
    //$('div#menu').accordion("option", "active", num);
}

Map.prototype.FocusOnAdditionalMenuItem = function (num) {
    var _num = $('div#menu div').length - this.additional_menu_items_count - 1 + num;
    this.FocusOnMenuItem(_num);
}

Map.prototype.NoFocusInMenu = function () {
    //$('div#menu').accordion("option", "active", "false");
}


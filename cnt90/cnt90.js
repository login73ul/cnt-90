function DeviceCNT90(node, width, height) {
    DeviceCNT90.superclass.constructor.call(this, node, width, height);
    this.state = new State();
    this.start_state = jQuery.extend(true, {}, this.state);
    this.menu_area_builder = new MenuAreaBuilder();
    this.menu_controls_builder = new MenuControlsBuilder();
    this.current_area = "menu_input";
    this.current_controls = "menu_input_controls";
    this.timer = null;
    this.time = 0;
    this.impulse = false;


    this.tmpState = jQuery.extend(true, {}, this.state);

    this.state_time_variables = ["text", "presintationModeOfValue", "v", "N", "Min", "P_P", "Std", "SumFreq", "PrevFreq", "M", "MinPM", "P_PPM", "AdevPM", "StdPM", "Max", "MaxPM", "t", "offset", "freq"];

    this.InitEx();
}

extend(DeviceCNT90, Map);


DeviceCNT90.prototype.saveToMemento = function () {
  return new Memento(this.state);
}

DeviceCNT90.prototype.restoreFromMemento = function (memento) {
  this.action(this.state, memento.getSavedState(), 'change_state');
  this.state_save();
  this.state = memento.getSavedState();
}


DeviceCNT90.prototype.numbers_area = function () {
	numbers_area = [];
	numbers_area.push({shape: "rect", key: "1", coords: "126,403,179,430", tooltip: "Цифра 1", hint_text: "1"});
	numbers_area.push({shape: "rect", key: "2", coords: "198,401,253,429", tooltip: "Цифра 2", hint_text: "2"});
	numbers_area.push({shape: "rect", key: "3", coords: "273,401,327,428", tooltip: "Цифра 3", hint_text: "3"});
	numbers_area.push({shape: "rect", key: "4", coords: "348,401,401,429", tooltip: "Цифра 4", hint_text: "4"});
	numbers_area.push({shape: "rect", key: "5", coords: "423,399,475,428", tooltip: "Цифра 5", hint_text: "5"});
	numbers_area.push({shape: "rect", key: "6", coords: "126,444,179,471", tooltip: "Цифра 6 / ВХОД А. Открывает меню, из которого можно регулировать все параметры настройки для входа А, такие как связь, входное полное сопротивление и затухание.", hint_text: "6"});
	numbers_area.push({shape: "rect", key: "7", coords: "200,445,253,472", tooltip: "Цифра 7 / ВХОД В. Открывает меню, из которого можно регулировать все параметры настройки для входа В, такие как связь, входное полное сопротивление и затухание. ", hint_text: "7"});
	numbers_area.push({shape: "rect", key: "8", coords: "272,444,328,471", tooltip: "Цифра 8 / ПАРАМЕТРЫ НАСТРОЙКИ. Параметры настройки доступные в этом меню зависят от функции измерения и входного канала. ", hint_text: "8"});
	numbers_area.push({shape: "rect", key: "9", coords: "349,444,403,472", tooltip: "Цифра 9 / МАТЕМАТИКА/ПРЕДЕЛЫ. Меню для выбора формулы из ряда формул для модификации результата измерения. Три постоянных величины могут вводиться через клавиатуру. Также могут вводиться численные пределы для сообщения о состоянии и регистрации. ", hint_text: "9"});
	numbers_area.push({shape: "rect", key: "0", coords: "423,443,475,471", tooltip: "Цифра 0 ", hint_text: "0"});
	
	return numbers_area;
}


DeviceCNT90.prototype.presentation_mode_area = function () {
	var label_area = [];
	label_area.push({shape: "rect", key: "characteristicLabel", coords: "102,209,227,229", tooltip: "", hint_text: "", font_size: 18, depth: 0, font_family: "Arial, Helvetica, sans-serif"});;
	label_area.push({shape: "rect", key: "limits", coords: "431,203,469,219", tooltip: "", hint_text: "", font_size: 12, depth: 0, font_family: "Arial, Helvetica, sans-serif"});;
  label_area.push({shape: "rect", key: "characteristicOutput", coords: "130,230,625,282", tooltip: "", hint_text: "", font_size: 40, depth: 0, font_family: "Arial, Helvetica, sans-serif"});;
	label_area.push({shape: "rect", key: "history_way", coords: "128,279,583,294", tooltip: "", hint_text: "", font_size: 10, depth: 0, font_family: "Arial, Helvetica, sans-serif"});;
	return label_area;
}

DeviceCNT90.prototype.initialize_area = function () {
	initialize_area = [];

	initialize_area.push({shape: "rect", key: "mainMonitor", coords: "100,203,640,366", tooltip: "ГРАФИЧЕСКИЙ ДИСПЛЕЙ ЖК-дисплей размером 320 х 97 пикселей с подсветкой для выводимых результатов измерений в численной и графической форме. Дисплей является центром динамического интерфейса пользователя, включающим деревья меню, индикаторы и информационные поля. ", hint_text: "Графический дисплей"});
	initialize_area.push({shape: "rect", key: "power", coords: "62,435,97,470", tooltip: "РЕЗЕРВНЫЙ РЕЖИМ / ВКЛ. Переключатель на вторичное напряжение. При нажатии этой кнопки в резервном режиме счетчик включается и восстанавливает все настройки, имевшиеся на момент выключения питания. ", hint_text: "Кнопка включения прибора"});
	initialize_area.push({shape: "rect", key: "socket", coords: "1000,447,1100,567", tooltip: "Розетка", hint_text: "Розетка"});

	return initialize_area;
}

DeviceCNT90.prototype.getArea = function () {
    var area = [];
    area = area.concat(this.initialize_area());
    area = area.concat(this.numbers_area());
    area = area.concat(this.presentation_mode_area());

    area.push({ shape: "rect", key: "emptyBtn1", coords: "497,401,550,428", tooltip: "КЛАВИШИ ДЛЯ ВВОДА ЧИСЛЕННЫХ ЗНАЧЕНИЙ. Время от времени необходимо вводить численные значения, такие как постоянные величины и пределы, запрашиваемые при использовании постобработки в режиме математики/пределов. Для этой цели служат указанные 12 клавиш. ", hint_text: "emptyBtn1" });
    area.push({ shape: "rect", key: "emptyBtn2", coords: "570,401,623,428", tooltip: "КЛАВИШИ ДЛЯ ВВОДА ЧИСЛЕННЫХ ЗНАЧЕНИЙ. Время от времени необходимо вводить численные значения, такие как постоянные величины и пределы, запрашиваемые при использовании постобработки в режиме математики/пределов. Для этой цели служат указанные 12 клавиш.", hint_text: "emptyBtn2" });
    area.push({ shape: "rect", key: "run", coords: "497,443,550,471", tooltip: "Остановить/выполнить. Служит для переключения (однократного) режима остановки на режим выполнения наоборот. Замораживает результат после выполнения измерения, если активен режим остановки ", hint_text: "run" });
    area.push({ shape: "rect", key: "restart", coords: "571,444,624,470", tooltip: "Повторный запуск. Запускает новое измерение, если активен режим остановки. ", hint_text: "restart" });
    //softkeys
    area.push({ shape: "rect", key: "measFunc", coords: "687,217,723,253", tooltip: "Дерево меню для выбора функции измерения. Для подтверждения Вы можете использовать семь клавиш с изменяемой функцией под дисплеем.", hint_text: "Дерево меню для выбора функции измерения. Для подтверждения Вы можете использовать семь клавиш с изменяемой функцией под дисплеем." });
    area.push({ shape: "rect", key: "autoSet", coords: "744,216,781,253", tooltip: "Автоматически согласует напряжение входного запускающего сигнала с оптимальными уровнями выбранной функции измерения.При двойном нажатии восстанавливаются настройки, используемые по умолчанию.", hint_text: "autoSet" });
    // Автоматически согласует напряжение входного запускающего сигнала с оптимальными уровнями выбранной функции измерения.При двойном нажатии восстанавливаются настройки, используемые по умолчанию. 
    area.push({ shape: "rect", key: "value", coords: "688,268,723,303", tooltip: "Вводит нормальный режим численной презентации с одним главным параметром и рядом вспомогательных параметров.", hint_text: "value" });
    area.push({ shape: "rect", key: "exit", coords: "745,268,781,304", tooltip: "Служит для подтверждения выбора в меню и выхода с уровня в дереве меню. ", hint_text: "exit" });
    area.push({ shape: "rect", key: "startPlot", coords: "688,319,723,354", tooltip: "Вводит один из трех режимов статистической презентации. Переключение режимов выполняется нажатием клавиши.", hint_text: "startPlot" });
    area.push({ shape: "rect", key: "cancel", coords: "745,319,781,355", tooltip: "Служит для выхода из меню без подтверждения произведенных изменений.", hint_text: "cancel" });
    //joystick//
    area.push({ shape: "circle", key: "up", coords: "873,231,15", tooltip: "Вверх. Позиция курсора выделяемая инверсией текста на дисплее, может изменяться в четырех направлениях. ", hint_text: "up" });
    area.push({ shape: "circle", key: "down", coords: "871,338,15", tooltip: "Вниз. Позиция курсора выделяемая инверсией текста на дисплее, может изменяться в четырех направлениях. ", hint_text: "down" });
    area.push({ shape: "circle", key: "right", coords: "927,285,15", tooltip: "Вправо. Позиция курсора выделяемая инверсией текста на дисплее, может изменяться в четырех направлениях. ", hint_text: "right" });
    area.push({ shape: "circle", key: "left", coords: "819,283,15", tooltip: "Влево. Позиция курсора выделяемая инверсией текста на дисплее, может изменяться в четырех направлениях. ", hint_text: "left" });
    area.push({ shape: "rect", key: "enter", coords: "852,265,893,306", tooltip: "Служит для подтверждения выбора в меню без выхода с урововня меню.", hint_text: "enter" });
    //indicators
    area.push({ shape: "circle", key: "indPower", coords: "83,408,6", tooltip: "сид «Резервный режим» Этот СИД светится, если счетчик находится в резервном режиме, указывая, что напряжение еще подается на внутренний факультативный генератор ОСХО (если он монтирован). ", hint_text: "indPower" });
    area.push({ shape: "circle", key: "indTrigA", coords: "687,384,6", tooltip: "ИНДИКАТОР ЗАПУСКА. Мигающий СИД указывает точность запуска ", hint_text: "indTrigA" });
    area.push({ shape: "circle", key: "indGate", coords: "737,384,6", tooltip: "ИНДИКАТОР ПРОПУСКАНИЯ При длящемся измерении этот СИД мигает", hint_text: "indGate" });
    area.push({ shape: "circle", key: "indTrigB", coords: "788,384,7", tooltip: "ИНДИКАТОР ЗАПУСКА. Мигающий СИД указывает точность запуска ", hint_text: "indTrigB" });
    //inputs
    area.push({ shape: "circle", key: "inputA", coords: "694,458,20", tooltip: "ГЛАВНЫЙ ВХОД А. Два идентичных канала А и В со связью по постоянному току используются для всех типов измерений как порознь, так и вместе. ", hint_text: "inputA" });
    area.push({ shape: "circle", key: "inputB", coords: "794,457,20", tooltip: "ГЛАВНЫЙ ВХОД B. Два идентичных канала А и В со связью по постоянному току используются для всех типов измерений как порознь, так и вместе. ", hint_text: "inputB" });
    area.push({ shape: "circle", key: "inputC", coords: "916,467,21", tooltip: "ВХОД ВЧ. (факультативный вход С) Могут поставляться предварительные делители частоты для различных частотных диапазонов. Эти устройства полностью автоматизированы и органы управления не влияют на технические характеристики. Соединитель типа N монтирован только, если внутри находится предварительный делитель. ", hint_text: "inputC" });
    area.push({ shape: "", key: "menu_bar", coords: "", tooltip: "", hint_text: "" });

    return area;
}


DeviceCNT90.prototype.setLabel = function() {
  main_label = this.GetControlByName("characteristicLabel");
  measured = this.state.currentCharacteristic;
  input_letter = this.state.activeInput.substr(this.state.activeInput.length - 1, 1);
  this.state.measuredCharacteristic = (measured + input_letter + ":");
}

DeviceCNT90.prototype.setInput = function (input) {
    this.state.activeInput = input;
    this.state.currentCharacteristic = "Frequency";
    this.setLabel();
}


DeviceCNT90.prototype.number_input = function(number) {
  this.menu_bar.number_input(number);
}

DeviceCNT90.prototype.numbers_controls = function () {
	numbers_controls = [];
	
	numbers_controls.push({key: '1', cls: ButtonCNT90, param: {cond: 's.power', act: 'this.map.number_input(1)'}});
	numbers_controls.push({key: '2', cls: ButtonCNT90, param: {cond: 's.power', act: 'this.map.number_input(2)'}});
	numbers_controls.push({key: '3', cls: ButtonCNT90, param: {cond: 's.power', act: 'this.map.number_input(3)'}});
	numbers_controls.push({key: '4', cls: ButtonCNT90, param: {cond: 's.power', act: 'this.map.number_input(4)'}});//'d.text = s.text+1'
	numbers_controls.push({key: '5', cls: ButtonCNT90, param: {cond: 's.power', act: 'this.map.number_input(5)'}});
	numbers_controls.push({key: '6', cls: ButtonCNT90, param: {cond: 's.power', act: 'this.map.number_input(6)'}});
	numbers_controls.push({key: '7', cls: ButtonCNT90, param: {cond: 's.power', act: 'this.map.number_input(7)'}});
	numbers_controls.push({key: '8', cls: ButtonCNT90, param: {cond: 's.power', act: 'this.map.number_input(8)'}});
	numbers_controls.push({key: '9', cls: ButtonCNT90, param: {cond: 's.power', act: 'this.map.number_input(9)'}});
	numbers_controls.push({key: '0', cls: ButtonCNT90, param: {cond: 's.power', act: 'this.map.number_input(0)'}});//'this.map.resetMenu("menu_settings");this.map.menu_bar.reconstructMenu("menu_settings")'}});
	return numbers_controls;
}


DeviceCNT90.prototype.presentation_mode_controls = function () {
	presentation_mode_controls = [];
	presentation_mode_controls.push({key: 'characteristicLabel', cls: MenuElement, param: {help_display: "none", cond: 'map.state.power', val: 'this.map.state.measuredCharacteristic'}});
	presentation_mode_controls.push({key: 'limits', cls: MenuElement, param: {help_display: "none", cond: 'map.state.power&&map.state.limits.indLimShow', val: '"LIM"'}});
	presentation_mode_controls.push({key: 'characteristicOutput', cls: MenuElement, param: {help_display: "none", cond: 'map.state.power', val: 'this.map.state.presintationModeOfValue'}});
	presentation_mode_controls.push({key: 'history_way', cls: MenuElement, param: {help_display: "none", cond: 'map.state.power', val: 'this.map.state.hist'}});
	return presentation_mode_controls;
}

DeviceCNT90.prototype.resetMenu = function () {
    for (var i = 0; i < this.controls.length; i++) {
        if (this.controls[i].name == "menu_bar") {
            this.menu_bar = this.controls[i];
        }
        if (this.controls[i].name == "generator") {
            this.generator = this.controls[i];
        }
    }
    this.menu_bar.reconstructMenu(this.state.current_menu_way);
    this.menu_bar.draw();
}

DeviceCNT90.prototype.setMenu = function (name) {
  this.state.current_menu_way = name;
}

DeviceCNT90.prototype.checkActiveInput = function() {
var selected_item = this.map.state.measurement;//this.map.menu_bar.menu_elements_configuration[this.map.menu_bar.current_pos_way][this.map.menu_bar.current_index].key;
  switch (selected_item) {
    case "meas_func_freq_input_a":
	  if (this.map.state.inputA.input) {
	    return true;
	  } else {
	    return false;
	  }
      break;
    case "meas_func_freq_input_b":
	  if (cnt90.state.inputB.input) {
	    return true;
	  } else {
	    return false;
	  }
      break;
    case "meas_func_freq_input_c":
	  if (cnt90.state.inputC.input) {
	    return true;
	  } else {
	    return false;
	  }
      break;
    case "meas_func_volt_input_a":
	  if (cnt90.state.inputA.input) {
	    return true;
	  } else {
	    return false;
	  }
      break;
    case "meas_func_volt_input_b":
	  if (cnt90.state.inputB.input) {
	    return true;
	  } else {
	    return false;
	  }
      break;
    case "meas_func_volt_input_c":
	  if (cnt90.state.inputC.input) {
	    return true;
	  } else {
	    return false;
	  }
      break;
    case "A/B":
	  if (cnt90.state.inputA.input && cnt90.state.inputB.input) {
	    return true;
	  } else {
	    return false;
	  }
      break;
    case "B/A":
	  if (cnt90.state.inputA.input && cnt90.state.inputB.input) {
	    return true;
	  } else {
	    return false;
	  }
      break;
    case "C/A":
	  if (cnt90.state.inputA.input && cnt90.state.inputC.input) {
	    return true;
	  } else {
	    return false;
	  }
      break;
    case "C/B":
	  if (cnt90.state.inputB.input && cnt90.state.inputC.input) {
	    return true;
	  } else {
	    return false;
	  }
      break;
    case "IntervalAtoB":
	  if (cnt90.state.inputA.input && cnt90.state.inputB.input) {
	    return true;
	  } else {
	    return false;
	  }
      break;
    case "IntervalBtoA":
	  if (cnt90.state.inputA.input && cnt90.state.inputB.input) {
	    return true;
	  } else {
	    return false;
	  }
      break;
    case "rise_input_a":
	  if (cnt90.state.inputA.input) {
	    return true;
	  } else {
	    return false;
	  }
      break;
    case "fall_input_a":
	  if (cnt90.state.inputA.input) {
	    return true;
	  } else {
	    return false;
	  }
      break;
    case "rise_input_b":
	  if (cnt90.state.inputB.input) {
	    return true;
	  } else {
	    return false;
	  }
      break;
    case "fall_input_b":
	  if (cnt90.state.inputB.input) {
	    return true;
	  } else {
	    return false;
	  }
      break;
    case "pulse_width_a":
	  if (cnt90.state.inputA.input) {
	    return true;
	  } else {
	    return false;
	  }
      break;
    case "pulse_width_b":
	  if (cnt90.state.inputB.input) {
	    return true;
	  } else {
	    return false;
	  }
      break;
    case "duty_a":
	  if (cnt90.state.inputA.input) {
	    return true;
	  } else {
	    return false;
	  }
      break;
    case "duty_b":
	  if (cnt90.state.inputB.input) {
	    return true;
	  } else {
	    return false;
	  }
      break;
    case "A_and_B":
	  if (cnt90.state.inputA.input && cnt90.state.inputB.input) {
	    return true;
	  } else {
	    return false;
	  }
      break;
    case "A_and_C":
	  if (cnt90.state.inputA.input && cnt90.state.inputC.input) {
	    return true;
	  } else {
	    return false;
	  }
      break;
    case "B_and_C":
	  if (cnt90.state.inputB.input && cnt90.state.inputC.input) {
	    return true;
	  } else {
	    return false;
	  }
      break;
  };
}

DeviceCNT90.prototype.definitionControl = function () {
	var controls = [];
	controls.push({key: 'mainMonitor', cls: ImageBox, param: {cond: 's.power', act: '', on: '', image: "cnt90/empty_monitor.png;550;165"}});
	//controls.push({key: 'power', cls: ButtonBoxImageCNT90, param: 'd.power=!s.power'});
	controls = controls.concat(this.numbers_controls());
	controls = controls.concat(this.presentation_mode_controls());

	controls.push({key: 'socket', cls: ButtonBoxImageCNT90, param: {act: 'if (s.socket) { cnt90.control("menu_bar").menuRectangeRedraw(); cnt90.setStatPlotInDefault(); } d.socket=!s.socket, d.power=0, this.map.generator.stopSignal(), this.map.generator.hideGraphPlot(), d.signalA.freq = 0, d.text = d.signalA.freq, cnt90.time = 0', on: 't.socket', image_on: "cnt90/fork.jpg;100;120", image_off: "cnt90/socket.jpg;100;120"}});
	controls.push({key: 'power', cls: ButtonBoxImageCNT90, param: { cond: 's.socket', act: 'd.power=!s.power; this.map.generator.stopSignal(), this.map.generator.hideGraphPlot(),  d.signalA.freq = 0, d.text = d.signalA.freq, this.map.setMenu("menu_meas_func"); this.map.resetMenu(); cnt90.time = 0'}});	
	
	controls.push({key: 'emptyBtn1', cls: ButtonCNT90, param: {cond: 's.power', act: 'this.map.menu_bar.first_empty_button_press();'}});
	controls.push({key: 'emptyBtn2', cls: ButtonCNT90, param: {cond: 's.power', act: 'this.map.menu_bar.second_empty_button_press();'}});
	controls.push({key: 'run', cls: ButtonBoxImageCNT90, param: {cond: '/*s.power&&cnt90.checkActiveInput();*/(s.power&&s.inputA.input)||(s.power&&s.inputB.input)||(s.power&&s.inputC.input)', act: 'this.map.generator.actionInput()'}});
	controls.push({key: 'restart', cls: ButtonCNT90, param: {cond: 's.power', act: 'this.map.menu_bar.sign_button_press();'}});
	
	controls.push({key: 'measFunc', cls: ButtonBoxImageCNT90, param: {cond: 's.power', act: 'this.map.setMenu("menu_meas_func"); this.map.resetMenu(); this.map.generator.stopSignal()'}});
	controls.push({key: 'autoSet', cls: ButtonCNT90, param: {cond: 's.power', act: 'd.text = "Auto set button"'}});
	controls.push({key: 'value', cls: ButtonBoxImageCNT90, param: {cond: '/*s.power&&cnt90.checkActiveInput();*/((s.power&&s.inputA.input)||(s.power&&s.inputB.input)||(s.power&&s.inputC.input))', act: 'this.map.generator.actionSignal()' }});
	controls.push({key: 'exit', cls: ButtonCNT90, param: {cond: 's.power', act: 'this.map.menu_bar.saveAndExit()'}});
	controls.push({key: 'startPlot', cls: ButtonBoxImageCNT90, param: {cond: 's.power', act: 'd.graphic.isDisplay=!s.graphic.isDisplay, this.map.generator.showGraphic(), this.map.generator.getState()'}});
	controls.push({key: 'cancel', cls: ButtonCNT90, param: {cond: 's.power', act: 'this.map.menu_bar.cancel()'}});
	
	controls.push({key: 'up', cls: ButtonCNT90, param: {cond: 's.power', act: 'this.map.menu_bar.getUp()'}});
	controls.push({key: 'down', cls: ButtonCNT90, param: {cond: 's.power', act: 'this.map.menu_bar.getDown()'}});
	controls.push({key: 'right', cls: ButtonBoxImageCNT90, param: {cond: 's.power', act: '/*this.map.generator.stopSignal(); d.text = 0, */this.map.menu_bar.getNext()'}});
	controls.push({key: 'left', cls: ButtonBoxImageCNT90, param: {cond: 's.power', act: '/*this.map.generator.stopSignal();  d.text = 0, */this.map.menu_bar.getPrev()'}});
	controls.push({key: 'enter', cls: ButtonBoxImageCNT90, param: {cond: 's.power', act: 'this.map.menu_bar.selectItem()'}});
	
	controls.push({key: 'indPower', cls: IndicatorDiode, param: CNT90.getStateIndicator});
	controls.push({key: 'indTrigA', cls: IndicatorDiode, param: CNT90.getStateIndicator});
	controls.push({key: 'indGate', cls: IndicatorDiode, param: CNT90.getStateIndicator});
	controls.push({key: 'indTrigB', cls: IndicatorDiode, param: CNT90.getStateIndicator});
	
	controls.push({key: 'inputA', cls: ButtonBoxImageCNT90, param: {cond: 's.power', act: 'd.inputA.input=!s.inputA.input, d.trigA=!s.trigA, this.map.generator.stopSignal(), d.signalA.freq = 0, d.text = d.signalA.freq', on: 't.power&&t.inputA.input', image_on: "cnt90/input.png;46;55", image_off: null}});
	controls.push({key: 'inputB', cls: ButtonBoxImageCNT90, param: {cond: 's.power', act: 'd.inputB.input=!s.inputB.input, d.trigB=!s.trigB, this.map.generator.stopSignal(), d.signalB.freq = 0, d.text = d.signalB.freq', on: 't.power&&t.inputB.input', image_on: "cnt90/input.png;46;55", image_off: null}});
	controls.push({key: 'inputC', cls: ButtonBoxImageCNT90, param: {cond: 's.power', act: 'd.inputC.input=!s.inputC.input, this.map.generator.stopSignal(), d.signalC.freq = 0, d.text = d.signalC.freq', on: 't.power&&t.inputC.input', image_on: "cnt90/input.png;46;55", image_off: null}});
	
	controls.push({key: 'generator', cls: CNT90.Generator});
	
	controls.push({key: 'menu_bar', cls: MenuBar, param: {cond: 's.power', act: ''}});
	
	//график
	controls.push({key: 'graph', cls: GraphVisio, param: {cond: 's.power', act: ''}});
	controls.push({key: 'tutor', cls: TutorCNT90, param: null});
	return controls;
}

DeviceCNT90.prototype.setStatPlotInDefault = function () {
    cnt90.state.stat_plot = { /*IsOn: false,*/ N: 0, Max: undefined, Min: undefined, P_P: 0, Adev: 0, Std: 0, N: 1, SumFreq: 0, PrevFreq: 0 };
}


var CNT90 = {}

CNT90.getStateIndicator = function (t, name) {
    m = {};
    if (name == 'indPower') {
        m['indPower'] = t.socket && !t.power;
    }
    if (name == 'indTrigA') {
        m['indTrigA'] = t.trigA && t.power;
    }
    if (name == 'indGate') {
        m['indGate'] = !t.gate && t.trigA && t.trigB && t.power;
    }
    if (name == 'indTrigB') {
        m['indTrigB'] = t.trigB && t.power;
    }
    return m[name];
}


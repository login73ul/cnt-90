function ButtonBoxImageCNT90(name, param) {
  ButtonBoxCNT90.superclass.constructor.call(this, name, param);
  this.param = param;
}
extend(ButtonBoxImageCNT90, ButtonCNT90);

ButtonBoxImageCNT90.prototype.setRender = function(render) {
	ButtonBoxImageCNT90.superclass.setRender.call(this, render);
}

ButtonBoxImageCNT90.prototype.setMap = function (map) {
	ButtonBoxImageCNT90.superclass.setMap.call(this, map);
	
	var b = this.box();
	if (this.param.image_on != null)
	{
		var a = this.param.image_on.split(';');
		this.param.pon = {src: a[0], w: a[1], h: a[2]};
	}
	
	if (this.param.image_off != null)
	{
		var a = this.param.image_off.split(';');
		this.param.poff = {src: a[0], w: a[1], h: a[2]};
	}
	
	var area = {shape: "image", coords: b.x + "," + b.y + "," + b.width + "," + b.height};
	area.tooltip = this.render.attr.tooltip;
	area.hint_text = this.render.attr.hint_text;
	if (this.param.top) {
		area.coords = (b.x + b.width/2)  + "," + b.y + ",5";
	}
	//this.render.remove();
	this.render = new Render(area, this);
	//this.render.getNode(map);
	map.addListener('state_change', this, function() {
		var t = map.state;
		m = eval(this.param.on);
		this.setLight(m);
	})
}

ButtonBoxImageCNT90.prototype.setLight = function (on) {
	this.light = on;
	if(on)
	{
		if (this.param.image_on != null)
		{
			var p = this.param.pon;
			this.render.setImage(p.src, p.w, p.h);
			this.render.opacity(1);
		}
		else
			this.render.opacity(0);
	}
	else
	{
		if (this.param.image_off != null)
		{
			var p = this.param.poff;
			this.render.setImage(p.src, p.w, p.h);
			this.render.opacity(1);
		}
		else
		{
			var b = this.box();
			this.render.node.attr({width:b.width, height: b.height});
			this.render.opacity(0);
		}
	}
}

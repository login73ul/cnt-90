function ButtonBoxCNT90(name, param) {
  ButtonBoxCNT90.superclass.constructor.call(this, name, param);
  this.param = param;
}
extend(ButtonBoxCNT90, ButtonCNT90);

ButtonBoxCNT90.prototype.setRender = function(render) {
	ButtonBoxCNT90.superclass.setRender.call(this, render);
}

ButtonBoxCNT90.prototype.setMap = function (map) {
	ButtonBoxCNT90.superclass.setMap.call(this, map);
	
	var b = this.box();
	var area = {shape: "circle", coords: (b.x + b.width/2)  + "," + (b.y + b.height/2) + ",5"}
	if (this.param.top) {
		area.coords = (b.x + b.width/2)  + "," + b.y + ",5";
	}
	this.renderDiode = new Render(area, this);
	this.renderDiode.getNode(map);
	
	map.addListener('state_change', this, function() {
		var t = map.state;
		m = eval(this.param.on);
		this.setLight(m);
	})
}

ButtonBoxCNT90.prototype.setLight = function (on) {
	this.light = on;
	if(on)
		this.renderDiode.lightOn();
	else
		this.renderDiode.lightOff();
}

var SOCKET = "Нажать на розетку <img class='button_img' id='socket' src='cnt90/training/socket.png'>";
var POWER = "Нажать кнопку включения <img class='button_img' id='power' src='cnt90/training/power.png'>";
var GENA = "Выбрать sin, v = 20mВ, freq = 100МГц для входа А <img class='button_img' id='gen' src='cnt90/training/generatorA.png'>";
var GENB = "Выбрать импульс, v = 20mВ, freq = 100МГц, длительность: 50 мкс для входа B <img class='button_img' id='gen' src='cnt90/training/generatorB.png'>";
var INPUTA = "Нажать на разьем A <img class='button_img' id='inputA' src='cnt90/training/inputA.png'>";
var INPUTB = "Нажать на разьем B <img class='button_img' id='inputB' src='cnt90/training/inputB.png'>";
var MEASFUNC = "Нажать кнопку MeasFunc <img class='button_img' id='measFunc' src='cnt90/training/measFunc.png'>";
var ENTER = "Нажать кнопку Enter <img class='button_img' id='enter' src='cnt90/training/enter.png'>";
var VALUE = "Нажать кнопку Value <img class='button_img' id='value' src='cnt90/training/value.png'>";
var STATPLOT = "Нажать кнопку  StatPlot <img class='button_img' id='startPlot' src='cnt90/training/startPlot.png'>";
var RUN = "Нажать кнопку  Hold/Run <img class='button_img' id='run' src='cnt90/training/run.png'>";
var B6 = "Нажать кнопку Input A(числовая клавиша 6) <img class='button_img' id='6' src='cnt90/training/6.png'>";

DeviceCNT90.prototype.InitEx = function () {
    this.exercises = [
		{
		    name: "Измерение частоты по входу А",
		    nodes: [],
		    control: "Включить прибор, настроить генератор, выбрав синусоидальный сигнал (v = 20mВ, freq = 100МГц). Подключить к разьему A. Запустить измерение частоты, вывести на экран значение и статистику. Отключить разьем и выключить прибор."
		},
        {
            name: "Изменение параметров настройки для входа А",
            nodes: [],
            control: "Включить прибор, настроить генератор, выбрав синусоидальный сигнал (v = 20mВ, freq = 100МГц). Подключить к разьему A. Установить параметры для входа A: отрицательный перепад, входное полное сопротивление 50 Ом, затухание 10x. Включить фильтр, выбрать аналоговый. Отключить разьем и выключить прибор."
        },
        {
            name: "Изменение напряжения А",
            nodes: [],
            control: "Включить прибор, настроить генератор, выбрав синусоидальный сигнал (v = 20mВ, freq = 100МГц). Подключить к разьему A. Запустить измерение напряжения, вывести на экран значение и статистику. Отключить разьем и выключить прибор."
        },
        {
            name: "Отношения частоты",
            nodes: [],
            control: "Включить прибор, настроить генератор, выбрав синусоидальный сигнал (v = 20mВ, freq = 100МГц). Подключить к разьему A. Подключить сигнал по входу B (импульс, v = 20mВ, freq = 100МГц). Запустить измерение отношения частоты (A/B), вывести на экран значение и статистику. Отключить разьемы и выключить прибор."
        }
    ]
    this.Ex1();
    this.Ex2();
    this.Ex3();
    this.Ex4();
}

DeviceCNT90.prototype.get_hash = function () {
    var foo = this.state_time_variables;
    return hex_md5(print_object(this.tmpState, function (arg) { return $.inArray(arg, foo) != -1 }));
}

DeviceCNT90.prototype.Ex1 = function () {
    //1
    this.tmpState = jQuery.extend(true, {}, this.state);
	this.tmpState.socket = true;	
	this.tmpState.button_push = "socket";
	this.tmpState.text = 0;
	this.exercises[0].nodes.push({
	    hash: this.get_hash(),
	    goal: "Включить прибор в сеть",
	    button_hint: SOCKET
	});
	console.log(this.tmpState);
	console.log("ручной" + hex_md5(print_object(this.tmpState)));
	//2
	this.tmpState.button_push = "power";
	this.tmpState.power = true;
	this.tmpState.hist = "menu_meas_func:";
	this.tmpState.current_menu_way = "menu_meas_func";
	this.exercises[0].nodes.push({
	    hash: this.get_hash(),
	    goal: "Включить питание",
	    button_hint: POWER
	});
    //3
	this.tmpState.genA = "sin|20|100000000|0|0";
	this.tmpState.button_push = "radio";
	this.exercises[0].nodes.push({
	    hash: this.get_hash(),
	    goal: "Настроить генератор",
	    button_hint: GENA
	});
    //3,5
	this.tmpState.inputA.input = true;
	this.tmpState.trigA = true;
	this.tmpState.hist = "menu_meas_func:";
	this.tmpState.button_push = "inputA";
	this.exercises[0].nodes.push({
	    hash: this.get_hash(),
	    goal: "Подключить сигнал по входу А",
	    button_hint: INPUTA
	});
	//4							   
	this.tmpState.button_push = "measFunc";
	this.exercises[0].nodes.push({
	    hash: this.get_hash(),
	    goal: "Перейти в меню измерения",
	    button_hint: MEASFUNC
	});
	//5
	this.tmpState.button_push = "enter";	
	this.tmpState.hist = "menu_meas_func:menu_meas_func_freq:";
	this.tmpState.current_menu_way = "menu_meas_func_freq";
	this.exercises[0].nodes.push({
	    hash: this.get_hash(),
	    goal: "В пункте меню meas_func выбрать вкладку <img src='cnt90/training/freq.png'>",
	    button_hint: ENTER
	});
    //6
	this.tmpState.hist = "menu_meas_func:menu_meas_func_freq:menu_meas_func_freq_freq:";
	this.tmpState.current_menu_way = "menu_meas_func_freq_freq";
	this.exercises[0].nodes.push({
	    hash: this.get_hash(),
	    goal: "В пункте меню meas_func_freq выбрать вкладку <img src='cnt90/training/freq.png'>",
	    button_hint: ENTER
	});
    // 7
	this.tmpState.signalA.typeSignal = "NaN";
	this.tmpState.measurement = "meas_func_freq_input_a";
	this.tmpState.current_pos_way = "menu_meas_func_freq";
    // генератор
	//this.tmpState.signalA.freq = 100000000;
	this.tmpState.signalA.offset = 0.01;
	this.tmpState.signalA.duration = 0.000000005;//0.5;
	//this.tmpState.signalA.t = 0.5;
    this.tmpState.signalA.typeSignal = "sin";
	this.tmpState.signalA.v = 0.02;

	//console.log("TMPSTATE: " + print_object(this.tmpState));
	this.exercises[0].nodes.push({
	    hash: this.get_hash(),
	    goal: "В пункте меню meas_func_freq_freq выбрать вкладку <img src='cnt90/training/input_a.png'>",
	    button_hint: ENTER
	});
		
    // 8
	this.tmpState.button_push = "run";
	this.tmpState.signalA.freq = undefined;
	this.tmpState.graphic.timeBetweenMeasurements = 0.0000000002;
	this.tmpState.startGenerator = "1";
	this.exercises[0].nodes.push({
	    hash: this.get_hash(),
	    goal: "Запустить измерение",
	    button_hint: RUN
	});
    //9
	this.tmpState.button_push = "value";
	this.tmpState.text = undefined;
	this.tmpState.presintationModeOfValue = undefined;
    // знак вопроса - это квадрат)
	this.tmpState.stat_plot.AdevPM = "0 Hz²";
	this.tmpState.stat_plot.StdPM = "0 Hz";
	this.exercises[0].nodes.push({
	    hash: this.get_hash(),
	    goal: "Значение",
	    button_hint: VALUE
	});
    //10
	this.tmpState.button_push = "startPlot";
	this.tmpState.hist = "menu_statplot_numerical:";
	this.tmpState.graphic.isDisplay = true;
	this.tmpState.current_menu_way = "menu_statplot_numerical";
	this.tmpState.current_menu_way = "menu_statplot_numerical";
	this.exercises[0].nodes.push({
	    hash: this.get_hash(),
	    goal: "Включить режим статистики",
	    button_hint: STATPLOT
	});
    //11 
	this.tmpState.trigA = false;
	this.tmpState.button_push = "inputA";
	this.tmpState.inputA.input = false;
	this.tmpState.measurement = "meas_func_freq_input_a";
	this.tmpState.current_pos_way = "menu_meas_func_freq";
	this.exercises[0].nodes.push({
	    hash: this.get_hash(),
	    goal: "Отключить сигнал по входу А",
	    button_hint: INPUTA
	});
    //12 
	this.tmpState.current_menu_way = "menu_meas_func";
	this.tmpState.hist = "menu_meas_func:";
	this.tmpState.power = false;
	this.tmpState.button_push = "power";
	this.exercises[0].nodes.push({
	    hash: this.get_hash(),
	    goal: "Отключить питание",
	    button_hint: POWER
	});
    //13
	this.tmpState.socket = false;
	this.tmpState.power = 0;
	this.tmpState.button_push = "socket";
	this.exercises[0].nodes.push({
	    hash: this.get_hash(),
	    goal: "Выключить прибор из сети",
	    button_hint: SOCKET
	});
}

DeviceCNT90.prototype.Ex2 = function () {
    //1
    this.tmpState = jQuery.extend(true, {}, this.state);
    this.tmpState.socket = true;
    this.tmpState.button_push = "socket";
    this.tmpState.text = 0;
    this.exercises[1].nodes.push({
        hash: this.get_hash(),
        goal: "Включить прибор в сеть",
        button_hint: SOCKET
    });
    //2
    this.tmpState.button_push = "power";
    this.tmpState.power = true;
    this.tmpState.hist = "menu_meas_func:";
    this.tmpState.current_menu_way = "menu_meas_func";
    this.exercises[1].nodes.push({
        hash: this.get_hash(),
        goal: "Включить питание",
        button_hint: POWER
    });
    //3
    this.tmpState.genA = "sin|20|100000000|0|0";
    this.tmpState.button_push = "radio";
    this.exercises[1].nodes.push({
        hash: this.get_hash(),
        goal: "Настроить генератор",
        button_hint: GENA
    });
    //4
    this.tmpState.inputA.input = true;
    this.tmpState.trigA = true;
    this.tmpState.hist = "menu_meas_func:";
    this.tmpState.button_push = "inputA";
    this.exercises[1].nodes.push({
        hash: this.get_hash(),
        goal: "Подключить сигнал по входу А",
        button_hint: INPUTA
    });
    //5
    this.tmpState.button_push = "6";
    this.tmpState.current_menu_way = "menu_input";
    this.tmpState.activeInput = "inputA";
    this.tmpState.hist = "menu_input:";
    this.tmpState.measuredCharacteristic = "FrequencyA:";
    this.exercises[1].nodes.push({
        hash: this.get_hash(),
        goal: "Перейти в меню параметров настройки Input A",
        button_hint: B6
    });
    //6
    this.tmpState.inputA.front = 'drop_plus';
    this.tmpState.measurement = "drop_plus";
    this.tmpState.button_push = "enter";
    this.tmpState.current_pos_way = null;
    this.exercises[1].nodes.push({
        hash: this.get_hash(),
        goal: "В пункте меню параметров изменить положительный перепад на отрицательный ",
        button_hint: "Нажать стрелку вниз, " + ENTER
    });
    //7 
    this.tmpState.measurement = "R50";
    this.tmpState.inputA.impedance = "R50";
    this.exercises[1].nodes.push({
        hash: this.get_hash(),
        goal: "В пункте меню параметров изменить входное полное сопротивление с 1МОм на 50 Ом",
        button_hint: "Нажать 2 раза стрелку вправо, нажать стрелку вниз, " + ENTER
    });
    //8 
    this.tmpState.measurement = "fading10";
    this.tmpState.inputA.attenuator = "fading10";
    this.tmpState.hist = "menu_input:";
    this.exercises[1].nodes.push({
        hash: this.get_hash(),
        goal: "В пункте меню параметров изменить затухание с 1х на 10х",
        button_hint: "Нажать стрелку вправо, нажать стрелку вверх, " + ENTER
    });
    //9
    this.tmpState.current_menu_way = "input_filter";
    this.tmpState.hist = "menu_input:input_filter:";
    this.exercises[1].nodes.push({
        hash: this.get_hash(),
        goal: "Включить фильтр",
        button_hint: "нажать три раза на стрелку вправо, " + ENTER
    });
    //10
    this.tmpState.inputA.filter = "an_lp";
    this.tmpState.current_menu_way = "lp_val";
    this.tmpState.hist = "menu_input:input_filter:lp_val:";
    this.exercises[1].nodes.push({
        hash: this.get_hash(),
        goal: "Выбрать аналоговый фильтр",
        button_hint: ENTER
    });
    //11 
    this.tmpState.trigA = false;
    this.tmpState.button_push = "inputA";
    this.tmpState.inputA.input = false;
    this.tmpState.measurement = "fading10";
    this.tmpState.current_pos_way = null;
    this.exercises[1].nodes.push({
        hash: this.get_hash(),
        goal: "Отключить сигнал по входу А",
        button_hint: INPUTA
    });
    //12 
    this.tmpState.current_menu_way = "menu_meas_func";
    this.tmpState.hist = "menu_meas_func:";
    this.tmpState.power = false;
    this.tmpState.button_push = "power";
    this.exercises[1].nodes.push({
        hash: this.get_hash(),
        goal: "Отключить питание",
        button_hint: POWER
    });
    //13
    this.tmpState.socket = false;
    this.tmpState.power = 0;
    this.tmpState.button_push = "socket";
    this.exercises[1].nodes.push({
        hash: this.get_hash(),
        goal: "Выключить прибор из сети",
        button_hint: SOCKET
    });
}

DeviceCNT90.prototype.Ex3 = function () {
    //1
    this.tmpState = jQuery.extend(true, {}, this.state);
    this.tmpState.socket = true;
    this.tmpState.button_push = "socket";
    this.tmpState.text = 0;
    this.exercises[2].nodes.push({
        hash: this.get_hash(),
        goal: "Включить прибор в сеть",
        button_hint: SOCKET
    });
    //2
    this.tmpState.button_push = "power";
    this.tmpState.power = true;
    this.tmpState.current_menu_way = "menu_meas_func";
    this.tmpState.hist = "menu_meas_func:";
    this.exercises[2].nodes.push({
        hash: this.get_hash(),
        goal: "Включить питание",
        button_hint: POWER
    });
    //3
    this.tmpState.genA = "sin|20|100000000|0|0";
    this.tmpState.button_push = "radio";
    this.exercises[2].nodes.push({
        hash: this.get_hash(),
        goal: "Настроить генератор",
        button_hint: GENA
    });
    //4
    this.tmpState.inputA.input = true;
    this.tmpState.trigA = true;
    this.tmpState.hist = "menu_meas_func:";
    this.tmpState.button_push = "inputA";
    this.exercises[2].nodes.push({
        hash: this.get_hash(),
        goal: "Подключить сигнал по входу А",
        button_hint: INPUTA
    });
    //5
    this.tmpState.button_push = "measFunc";
    this.tmpState.hist = "menu_meas_func:";
    this.exercises[2].nodes.push({
        hash: this.get_hash(),
        goal: "Перейти в меню измерения",
        button_hint: MEASFUNC
    });
    ////6
    this.tmpState.current_menu_way = "menu_meas_func_volt";
    this.tmpState.button_push = "enter";
    this.tmpState.hist = "menu_meas_func:menu_meas_func_volt:";
    this.exercises[2].nodes.push({
        hash: this.get_hash(),
        goal: "В пункте меню meas_func выбрать вкладку Volt",
        button_hint: "Для этого необходимо три раза нажать на стрелочку вправо и выбрать выбрать вкладку Volt <img src='cnt90/training/volt.png'> " + ENTER
    });
    // 6.5
    this.tmpState.current_menu_way = "menu_meas_func_volt";
    this.tmpState.measurement = "meas_func_volt_input_a";
    this.tmpState.current_pos_way = "menu_meas_func";
    this.tmpState.signalA.typeSignal = "sin";
    this.tmpState.signalA.v = 0.02;
    this.tmpState.signalA.duration = 0.000000005;

    this.tmpState.graphic.curInput = "meas_func_volt_input_a";
    this.tmpState.hist = "menu_meas_func:menu_meas_func_volt:";

    this.exercises[2].nodes.push({
        hash: this.get_hash(),
        goal: "Выбрать InputA",
        button_hint: ENTER
    });

    //7
    this.tmpState.button_push = "run";
    this.tmpState.startGenerator = "1";
	  this.tmpState.graphic.timeBetweenMeasurements = 0.0000000002;
    this.tmpState.meas_ment = "V";
    this.tmpState.hist = "menu_meas_func:menu_meas_func_volt:";
    this.exercises[2].nodes.push({
        hash: this.get_hash(),
        goal: "Запустить измерение",
        button_hint: RUN
    });
    //8
    this.tmpState.button_push = "value";
    this.tmpState.hist = "menu_meas_func:menu_meas_func_volt:";
    this.exercises[2].nodes.push({
        hash: this.get_hash(),
        goal: "Значение",
        button_hint: VALUE
    });
    //9 
    this.tmpState.button_push = "startPlot";
    this.tmpState.graphic.isDisplay = true;
    this.tmpState.current_menu_way = "menu_statplot_numerical";
    this.tmpState.hist = "menu_statplot_numerical:";
    this.exercises[2].nodes.push({
        hash: this.get_hash(),
        goal: "Включить режим статистики",
        button_hint: STATPLOT
    });
    ////10 
    this.tmpState.signalA.freq = 0;
    this.tmpState.button_push = "inputA";
    this.tmpState.inputA.input = false;
    this.tmpState.trigA = false;
    this.exercises[2].nodes.push({
        hash: this.get_hash(),
        goal: "Отключить сигнал по входу А",
        button_hint: INPUTA
    });
    //11 
    this.tmpState.current_menu_way = "menu_meas_func";
    this.tmpState.hist = "menu_meas_func:";
    this.tmpState.power = false;
    this.tmpState.button_push = "power";
    this.exercises[2].nodes.push({
        hash: this.get_hash(),
        goal: "Отключить питание",
        button_hint: POWER
    });
    //12 
    this.tmpState.socket = false;
    this.tmpState.power = 0;
    this.tmpState.button_push = "socket";
    this.exercises[2].nodes.push({
        hash: this.get_hash(),
        goal: "Выключить прибор из сети",
        button_hint: SOCKET
    });
}

DeviceCNT90.prototype.Ex4 = function () {
    //1
    this.tmpState = jQuery.extend(true, {}, this.state);
    this.tmpState.socket = true;
    this.tmpState.button_push = "socket";
    this.tmpState.text = 0;
    this.exercises[3].nodes.push({
        hash: this.get_hash(),
        goal: "Включить прибор в сеть",
        button_hint: SOCKET
    });
    //2
    this.tmpState.button_push = "power";
    this.tmpState.power = true;
    this.tmpState.hist = "menu_meas_func:";
    this.tmpState.current_menu_way = "menu_meas_func";
    this.exercises[3].nodes.push({
        hash: this.get_hash(),
        goal: "Включить питание",
        button_hint: POWER
    });
    //3
    this.tmpState.genA = "sin|20|100000000|0|0";
    this.tmpState.button_push = "radio";
    this.exercises[3].nodes.push({
        hash: this.get_hash(),
        goal: "Настроить генератор",
        button_hint: GENA
    });
    //4
    this.tmpState.inputA.input = true;
    this.tmpState.trigA = true;
    this.tmpState.hist = "menu_meas_func:";
    this.tmpState.button_push = "inputA";
    this.exercises[3].nodes.push({
        hash: this.get_hash(),
        goal: "Подключить сигнал по входу А",
        button_hint: INPUTA
    });
    //5
    this.tmpState.genB = "impulse|20|100000000|0|0";
    this.tmpState.button_push = "radio";
    this.exercises[3].nodes.push({
        hash: this.get_hash(),
        goal: "Настроить генератор",
        button_hint: GENB
    });
    //6
    this.tmpState.inputB.input = true;
    this.tmpState.trigB = true;
    this.tmpState.hist = "menu_meas_func:";
    this.tmpState.button_push = "inputB";
    this.exercises[3].nodes.push({
        hash: this.get_hash(),
        goal: "Подключить сигнал по входу B",
        button_hint: INPUTB
    });
    //7
    this.tmpState.button_push = "measFunc";
    this.tmpState.current_menu_way = "menu_meas_func";
    this.exercises[3].nodes.push({
        hash: this.get_hash(),
        goal: "Перейти в меню измерения",
        button_hint: MEASFUNC
    });
    //8
    this.tmpState.button_push = "enter";
    this.tmpState.hist = "menu_meas_func:menu_meas_func_freq:";
    this.tmpState.current_menu_way = "menu_meas_func_freq";
    this.exercises[3].nodes.push({
        hash: this.get_hash(),
        goal: "В пункте меню meas_func выбрать вкладку Freq (Frequency)",
        button_hint: ENTER
    });
    //9
    this.tmpState.current_menu_way = "menu_freq_ratio";
    this.tmpState.hist = "menu_meas_func:menu_meas_func_freq:menu_freq_ratio:";
    this.exercises[3].nodes.push({
        hash: this.get_hash(),
        goal: "В пункте меню meas_func_freq выбрать, используя стрелочку вправо, вкладку Freq Ratio",
        button_hint: ENTER
    });
    //10
    this.tmpState.signalB.typeSignal = "impulse";
    this.tmpState.signalA.typeSignal = "sin";
    this.tmpState.current_pos_way = "menu_meas_func_freq";
    this.tmpState.measurement = "A/B";
    this.tmpState.graphic.curInput = "A/B";
    this.tmpState.signalA.v = 0.02;
    this.tmpState.signalA.duration = 0.000000005;
    this.tmpState.signalB.v = 0.02;
    this.tmpState.signalB.duration = 0.000000005;
    this.exercises[3].nodes.push({
        hash: this.get_hash(),
        goal: "В пункте меню meas_func_freq_ratio выбрать вкладку А/В",
        button_hint: ENTER
    });
    //11
    this.tmpState.meas_ment = "";
    this.tmpState.button_push = "run";
    this.tmpState.startGenerator = "1";
    this.exercises[3].nodes.push({
        hash: this.get_hash(),
        goal: "Запустить измерение",
        button_hint: RUN
    });
    //12
    this.tmpState.button_push = "value";
    this.exercises[3].nodes.push({
        hash: this.get_hash(),
        goal: "Значение",
        button_hint: VALUE
    });
    //13
    this.tmpState.button_push = "startPlot";
    this.tmpState.hist = "menu_statplot_numerical:";
    this.tmpState.graphic.isDisplay = true;
    this.tmpState.current_menu_way = "menu_statplot_numerical";
    this.exercises[3].nodes.push({
        hash: this.get_hash(),
        goal: "Включить режим статистики",
        button_hint: STATPLOT
    });
    //14 
    this.tmpState.signalA.freq = 0;
    this.tmpState.button_push = "inputA";
    this.tmpState.inputA.input = false;
    this.tmpState.trigA = false;
    this.exercises[3].nodes.push({
        hash: this.get_hash(),
        goal: "Отключить сигнал по входу А",
        button_hint: INPUTA
    });
    //15 
    this.tmpState.signalB.freq = 0;
    this.tmpState.button_push = "inputB";
    this.tmpState.inputB.input = false;
    this.tmpState.trigB = false;
    this.exercises[3].nodes.push({
        hash: this.get_hash(),
        goal: "Отключить сигнал по входу B",
        button_hint: INPUTB
    });
    //16 
    this.tmpState.current_menu_way = "menu_meas_func";
    this.tmpState.hist = "menu_meas_func:";
    this.tmpState.power = false;
    this.tmpState.button_push = "power";
    this.exercises[3].nodes.push({
        hash: this.get_hash(),
        goal: "Отключить питание",
        button_hint: POWER
    });
    //17 
    this.tmpState.socket = false;
    this.tmpState.power = 0;
    this.tmpState.button_push = "socket";
    this.exercises[3].nodes.push({
        hash: this.get_hash(),
        goal: "Выключить прибор из сети",
        button_hint: SOCKET
    });
}

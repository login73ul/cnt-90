function MenuBar(name, param){
    this.menu_elements_configuration = {
        'menu_bar': [{ key: 'drop_minus' }, { key: 'drop_plus' }, { key: 'dc' }, { key: 'ac' }, { key: 'R50' }, { key: 'R1' }, { key: 'fading1' }, { key: 'fading10' }, { key: 'start_man' }, { key: 'start_auto' }, { key: 'trig_input_a' }, { key: 'filter_input_a' }],
        'menu_meas_func': [{ key: 'Freq', submenu: 'menu_meas_func_freq' },
                           { key: 'Time', submenu: 'menu_meas_func_time' },
                           { key: 'Phase', submenu: 'menu_meas_func_phase' },
                           { key: 'Volt', submenu: 'menu_meas_func_volt' }],
        'menu_meas_func_phase': [{key: "A_and_B"}, 
                                 {key: "A_and_C"},
                                 {key: "B_and_C"}],
        'menu_meas_func_volt': [{key: "meas_func_volt_input_a"},
                                {key: "meas_func_volt_input_b"},
                                {key: "meas_func_volt_input_c"}],
        'menu_meas_func_freq': [{key: "meas_func_freq", submenu: 'menu_meas_func_freq_freq'},
                                {key: "meas_func_freq_rat", submenu: 'menu_freq_ratio'},
                                {key: "meas_func_brust"}],
        'menu_meas_func_time': [{key: "meas_func_time_interval", submenu: 'time_interval'},
                                {key: "meas_func_rise_fall_time", submenu: 'rise_fall_time'},
                                {key: "meas_func_pulse_width", submenu:'pulse_width'},
                                {key: "meas_func_duty", submenu:'duty'}],
        'time_interval': [{key: "IntervalAtoB"}, {key: "IntervalBtoA"}],
        'rise_fall_time': [{key: "rise_input_a"}, {key: "fall_input_a"}, {key: "rise_input_b"}, {key: "fall_input_b"}],
        'pulse_width': [{key: "pulse_width_a"}, {key: "pulse_width_b"}],
        'duty': [{key: "duty_a"}, {key: "duty_b"}],
        'menu_meas_func_freq_freq': [{ key: "meas_func_freq_input_a" }, { key: "meas_func_freq_input_b" }, { key: "meas_func_freq_input_c" }],
        'menu_settings': [{ key: 'meas_time' }, { key: 'burst', submenu: 'menu_set_burst' }, { key: 'arm', submenu: 'menu_set_arm' }, { key: 'trigger_hold_off', submenu: 'menu_set_trigger_hold' }, { key: 'stat', submenu: 'menu_set_stat' }, { key: 'timebase_ref', submenu: 'menu_set_timebase' }, { key: 'misc', submenu: 'menu_set_misc' }],
        'menu_input': [{ key: 'drop_minus', option: "front" }, { key: 'drop_plus', option: "front" }, { key: 'dc', option: "coupling" }, { key: 'ac', option: "coupling" }, { key: 'R1', option: "impedance" }, { key: 'R50', option: "impedance" }, { key: 'fading10', option: "attenuator" }, { key: 'fading1', option: "attenuator" }, { key: 'start_auto', option: "triggerLevel" }, { key: 'start_man', option: "triggerLevel" }, { key: 'trig_input'}, { key: 'filter_input', submenu: 'input_filter' }],

        'input_filter': [{ key: 'an_lp', submenu: 'lp_val', option: "filter" },
                 { key: 'dig_lp', submenu: 'lp_val', option: "filter" },
                 { key: 'dig_lp_freq', submenu: 'lp_val', option: "filter" }],
        'lp_val': [{ key: "name_lp" }, { key: "count_hz" }, { key: "lp_hz" }, { key: "lp_mhz" }],

        'menu_freq_ratio': [{ key: "A/B" }, { key: "B/A" }, { key: "C/A" }, { key: "C/B" }],
        'menu_statplot_numerical': [{ key: "stat_plot_mean_label" }, { key: "stat_plot_mean" }, { key: "stat_plot_max_label" }, { key: "stat_plot_max" }, { key: "stat_plot_min_label" }, { key: "stat_plot_min" },
                                    { key: "stat_plot_p_p_label" }, { key: "stat_plot_p_p" }, { key: "stat_plot_adev_label" }, { key: "stat_plot_adev" }, { key: "stat_plot_std_label" }, { key: "stat_plot_std" }],
        'menu_statplot_trendplot': [{ key: "trend" }, { key: "upper_lim" }, { key: "lower_lim" }, { key: "measure" }, { key: "time" }],

        'menu_set_burst': [{ key: 'sync_delay' }, { key: 'start_delay' }, { key: 'brust_meas_time' }, { key: 'freq_limit' }],
        'menu_set_arm': [{ key: 'arm_start_chan_off' }, { key: 'arm_start_slope' }, { key: 'arm_start_arm_delay' }, { key: 'arm_stop_chan_off' }, { key: 'arm_stop_slope' }],
        'menu_set_trigger_hold': [{ key: 'hold_off_off' }, { key: 'set_trigger_hold_off' }],
        'menu_set_stat': [{ key: 'stat_sync_delay' }, { key: 'stat_start_delay' }, { key: 'stat_burst_meas_time' }, { key: 'stat_freq_limit' }],
        'menu_set_timebase': [{ key: 'timebase_int' }, { key: 'timebase_ext' }, { key: 'timebase_auto' }],
        'menu_set_misc': [{ key: 'misc_sync_delay' }, { key: 'misc_start_delay' }, { key: 'misc_burst_meas_time' }, { key: 'misc_freq_limit' }],
        'menu_math_limit': [{ key: 'menu_math', submenu: 'math' }, { key: 'menu_limit', submenu: 'limits' }],
        'math': [{ key: 'math_math', submenu: 'math_math' }, { key: 'math_k' }, { key: 'math_l' }, { key: 'math_m' }],
        'math_math': [{ key: 'math_math_off' }, { key: 'math_func1' }, { key: 'math_func2' }, { key: 'math_func3' }, { key: 'math_func4' }],
        'limits': [{ key: 'limit_beh', submenu: 'limits_state' }, { key: 'limit_mode', submenu: 'limits_mode' }, { key: 'low_lim', submenu: "math_constant" }, { key: 'upp_lim', submenu: "math_constant" }],
        'limits_state': [{ key: 'limit_beh_off', option: "state", value: "OFF", state_pos: "limits"}, { key: 'limit_beh_capture', option: "state", value: "CAPTURE", state_pos: "limits"}, { key: 'limit_beh_alarm', option: "state", value: "ALARM", state_pos: "limits" }, { key: 'limit_beh_alarm_stop', option: "state", value: "ALARM_STOP", state_pos: "limits" }],
        'limits_mode': [{ key: 'limit_mode_above', option: "mode", value: "ABOVE", state_pos: "limits"}, { key: 'limit_mode_below', option: "mode", value: "BELOW", state_pos: "limits"}, { key: 'limit_mode_range', option: "mode", value: "RANGE", state_pos: "limits"}],
        'math_constant': [{key: "constant"}, {key: "count_mantis"}, {key: "count_ee"}, {key: "count_exp"}, {key: "menu_ee"}, {key: "menu_const_ok"}]
    }

  this.menu_elements = [];
  this.way = [];
  this.way.push("menu_meas_func");  //стартуем с этой кнопки
  this.current_index = 0;
  this.current_pos_way = "menu_input";
  this.menu_area_builder = new MenuAreaBuilder();
  this.menu_controls_builder = new MenuControlsBuilder();
  MenuBar.superclass.constructor.call(this, name, param);
}

extend(MenuBar, Control);


MenuBar.prototype.setNavigator = function (navigator) {
  this.navigator = navigator;
}

MenuBar.prototype.number_input = function (number) {
  this.navigator.number_input(number);
}

MenuBar.prototype.initializeNavigator = function() {
  this.setNavigator(new OneLevelMenuBehaviour(this));
  switch(this.current_pos_way) {
    case "menu_input":
      this.setNavigator(new TwoLevelMenuBehaviour(this));
      break;
    case "math_constant":
      this.setNavigator(new MantissaMenuBehaviour(this));
      break;
  }
}

MenuBar.prototype.menuRectangeRedraw = function () {
  if(this.menu_elements.length == 0) {
    return;
  }
  this.menu_elements[this.current_index].deactivate();
  for(var i = 0; i < this.menu_elements.length; i++) {
    this.menu_elements[i].render.opacity(0);
  }
}

MenuBar.prototype.findPreviousMenu = function (way_to_find) {
  var previous_menu = null;
  if(way_to_find == null) {
    way_to_find = this.current_pos_way;
  }
  for(var property in this.menu_elements_configuration) {
    var routing = this.menu_elements_configuration[property]
    for(var i = 0; i < routing.length; i++) {
      if(routing[i].submenu == way_to_find && routing[i].previous_menu == routing[i].key) {
        return property;
      }
    }
  }
  return previous_menu;
}

MenuBar.prototype.initializeControlAndSetupRender = function() {
  this.menu_controls_builder.setMap(this.map);
  view_description = this.menu_area_builder[this.current_pos_way].call(this.menu_area_builder);
  model_description = this.menu_controls_builder[this.current_pos_way + "_controls"].call(this.menu_controls_builder);

  for(var i = 0; i < model_description.length; i++) {
    menu_element = new MenuElement (model_description[i].key, model_description[i].param)
    this.menu_elements.push(menu_element);
    var render = new Render(view_description[i], menu_element);
    menu_element.setRender(render);
    menu_element.setMap(this.map);
  }
}


MenuBar.prototype.reconstructMenu = function(name) {
  this.menuRectangeRedraw();
  this.menu_elements = [];
  this.current_index = 0;
  this.current_pos_way = name;
  this.initializeControlAndSetupRender();
  this.initializeNavigator();
  if (this.map.menu_bar.current_pos_way != "menu_statplot_numerical") {
    this.menu_elements[this.current_index].activate();
  }
  this.initial_memento = this.map.saveToMemento();
  this.drawWay();
}

MenuBar.prototype.haveArea = function () {
  return true;
}

MenuBar.prototype.currentRoute = function() {
  return this.menu_elements_configuration[this.current_pos_way];
}

MenuBar.prototype.stateToMenuMatching = function(menu_element) {
  if(this.map.state.activeInput == null) {
    return;
  }
  for(var i = 0; i < this.currentRoute().length; i++) {
    if(this.currentRoute()[i].value == undefined) {
      menu_element_value_equality_to_state = menu_element.name == this.currentRoute()[i].key;
    } else {
      menu_element_value_equality_to_state = menu_element.name == this.currentRoute()[i].value;
    }
    menu_element_option_equality_to_state = this.map.state[this.map.state.activeInput][this.currentRoute()[i].option] == menu_element.name;
    if(menu_element_value_equality_to_state && menu_element_option_equality_to_state) {
      menu_element.makeStroke();
    }
  }
}

MenuBar.prototype.draw = function () {
  for(var i = 0; i < this.menu_elements.length; i++) {
    this.menu_elements[i].draw();
    this.stateToMenuMatching(this.menu_elements[i]);
  }

  if (this.map.menu_bar.current_pos_way != "menu_statplot_numerical") {
    this.menu_elements[this.current_index].activate();
    this.resetActiveElement(this.current_index);
  }
  console.log("HERE");
  console.log(this.menu_elements);
}

MenuBar.prototype.isStatePosPresent = function (new_index) {
  return this.menu_elements_configuration[this.current_pos_way][new_index].state_pos != undefined;
}

MenuBar.prototype.isValuePresent = function (new_index) {
  return this.menu_elements_configuration[this.current_pos_way][new_index].value != undefined;
}

MenuBar.prototype.resetActiveElement = function(new_index) {
  current_config = this.menu_elements_configuration[this.current_pos_way][new_index];
  if(current_config.option != undefined && this.map != undefined) {
    if(this.map.state.activeInput != null) {
      this.map.state[this.map.state.activeInput][current_config.option] = current_config.key;
    }
    if ( this.isStatePosPresent(new_index) && this.isValuePresent(new_index)) {
      this.map.state[current_config.state_pos][current_config.option] = current_config.value;
    }
  }
  this.menu_elements[this.current_index].deactivate();
  this.current_index = new_index;

  if (this.map.menu_bar.current_pos_way != "menu_statplot_numerical") {
    this.menu_elements[new_index].activate();
  }
}

MenuBar.prototype.cancel = function () {
  this.map.restoreFromMemento(this.initial_memento);
  prev_menu = this.findPreviousMenu();
  if (prev_menu != null) {
    this.map.setMenu(prev_menu);
    this.map.resetMenu();
  }
}

MenuBar.prototype.saveAndExit = function () {
  prev_menu = this.findPreviousMenu();
  if(prev_menu != null) {
    this.map.setMenu(prev_menu);
    this.map.resetMenu();
  }
}

MenuBar.prototype.exit = function () {
}

MenuBar.prototype.selectItem = function() {
  this.navigator.selectItem();
}

MenuBar.prototype.first_empty_button_press = function() {
  this.navigator.first_empty_button_press();
}

MenuBar.prototype.second_empty_button_press = function() {
  this.navigator.second_empty_button_press();
}

MenuBar.prototype.sign_button_press = function() {
  this.navigator.sign_button_press();
}

MenuBar.prototype.drawWay = function() {
  var way = [];
  way.push(this.current_pos_way);
  prev_menu = this.findPreviousMenu();
  while (prev_menu != null) {
    way.push(prev_menu);
    prev_menu = this.findPreviousMenu(prev_menu);
  }
  way = way.reverse();
  current_way = "";
  for (var item_way in way) {
    current_way += way[item_way] + ':';
  }
  this.map.state.hist = current_way;
}


MenuBar.prototype.getPrev = function() {
  this.navigator.getPrev();
}

MenuBar.prototype.getNext = function() {
  this.navigator.getNext();
}

MenuBar.prototype.getUp = function() {
  this.navigator.getUp();
}

MenuBar.prototype.getDown = function() {
  this.navigator.getDown();
}

MenuBar.prototype.setMap = function(map) {
  MenuBar.superclass.setMap.call(this, map);
}

MenuBar.prototype.drawControl = function () {
  this.menuRectangeRedraw();
}

MenuBar.prototype.box = function () {
  return this.renderControl.getNode(this.map).getBBox();
}

MenuBar.prototype.stateChangePerform = function (state, new_state, action) {
  var b = this;
  var map = this.map;

  var s = state;
  var d = new_state;
  if (typeof(action) === 'string') {
    eval(action);
  } else {
    var cond = 1;
    if (action.cond != undefined) {
      cond = eval(action.cond);
    }
    if (cond) eval(action.act);
  }
}

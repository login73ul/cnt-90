function TwoLevelMenuBehaviour(menu) {
  TwoLevelMenuBehaviour.superclass.constructor.call(this, menu);
}
extend(TwoLevelMenuBehaviour, BaseMenuBehaviour);

TwoLevelMenuBehaviour.prototype.getPrev = function () {
  menu = this.menu;
  var new_index = menu.current_index;
  if(new_index == 1) {
    return;
  }
  var current_option = menu.menu_elements_configuration[menu.current_pos_way][menu.current_index].option;
  var new_option = undefined;
  do {
    new_index--;
    if(new_index < 0) {
      new_index = 0;
      break;
    }
    new_option = menu.menu_elements_configuration[menu.current_pos_way][new_index].option;
  } while (menu.map.state[menu.map.state.activeInput][new_option] != menu.currentRoute()[new_index].key && new_option != undefined)
  menu.resetActiveElement(new_index);
  menu.draw();
}

TwoLevelMenuBehaviour.prototype.getNext = function() {
  menu = this.menu;
  var new_index = menu.current_index;
  var current_option = menu.menu_elements_configuration[menu.current_pos_way][menu.current_index].option;
  var new_option = undefined;
  do {
    new_index++;
    if(new_index >= menu.menu_elements.length) {
      new_index = menu.menu_elements.length - 1;
      break;
    }
    if(current_option == undefined) {
      break;
    }
    new_option = menu.menu_elements_configuration[menu.current_pos_way][new_index].option;
    //alert(menu.map.state.activeInput);
  } while (menu.map.state[menu.map.state.activeInput][new_option] != menu.currentRoute()[new_index].key && new_option != undefined)
  menu.resetActiveElement(new_index);
  menu.draw();
}

TwoLevelMenuBehaviour.prototype.getUp = function() {
  menu = this.menu;
  var new_index = menu.current_index;
  var current_element = menu.menu_elements_configuration[menu.current_pos_way][menu.current_index];
  new_index--;
  var new_element = menu.menu_elements_configuration[menu.current_pos_way][new_index];
  if(new_element == undefined || new_element.option == undefined) {
    return;
  }
  if(new_index < 0) {
    new_index = 0;
  }
  if(current_element.option != new_element.option) {
    return;
  }
  menu.resetActiveElement(new_index);
  menu.draw();
}

TwoLevelMenuBehaviour.prototype.getDown = function() {
  menu = this.menu;
  var new_index = menu.current_index;
  var current_element = menu.menu_elements_configuration[menu.current_pos_way][menu.current_index];
  new_index++;
  var new_element = menu.menu_elements_configuration[menu.current_pos_way][new_index];
  if(new_element == undefined || new_element.option == undefined) {
    return;
  }
  if(new_index < 0) {
    new_index = 0;
  }
  if(current_element.option != new_element.option) {
    return
  }
  menu.resetActiveElement(new_index);
  menu.draw();
}

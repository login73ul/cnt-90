function menu_settings() {
	var area = [];
	area.push({shape: "rect", key: "meas_time", coords: "109,336,194,364", hint_text: "MeasTime 10ms"});
	area.push({shape: "rect", key: "burst", coords: "194,336,249,364", hint_text: "Burst"});
	area.push({shape: "rect", key: "arm", coords: "250,336,295,364", hint_text: "Arm"});
	area.push({shape: "rect", key: "trigger_hold_off", coords: "297,336,417,363", hint_text: "Trigger Hold Off (Off)"});
	area.push({shape: "rect", key: "stat", coords: "417,336,464,363", hint_text: "Stat"});
	area.push({shape: "rect", key: "timebase_ref", coords: "465,336,603,364", hint_text: "Timebase Ref (Int)"});
	area.push({shape: "rect", key: "misc", coords: "604,336,645,364", hint_text: "Misc"});
	return area;
}

function menu_set_burst() {
	var area = [];
	area.push({shape: "rect", key: "burst_sync_delay", coords: "108,336,241,364", hint_text: "Sync delay 200us"});
	area.push({shape: "rect", key: "burst_start_delay", coords: "243,336,373,363", hint_text: "Start delay 200us"});
	area.push({shape: "rect", key: "burst_meas_time", coords: "375,336,515,363", hint_text: "Meas Time 200us"});
	area.push({shape: "rect", key: "burst_freq_limit", coords: "517,336,645,363", hint_text: "Frequency Limit 300MHz"});
	return area;
}

function menu_set_arm() {
	var area = [];
	area.push({shape: "rect", key: "arm_start_chan_off", coords: "108,336,202,364", hint_text: "Start Chan Off"});
	area.push({shape: "rect", key: "arm_start_slope", coords: "203,336,289,363", hint_text: "Start Slope"});
	area.push({shape: "rect", key: "arm_start_arm_delay", coords: "290,336,449,363", hint_text: "Start Arm delay 200us"});
	area.push({shape: "rect", key: "arm_stop_chan_off", coords: "451,336,545,363", hint_text: "Stop Chan Off"});
	area.push({shape: "rect", key: "arm_stop_slope", coords: "547,336,645,363", hint_text: "Stop Slope"});
	return area;
}
function menu_set_trigger_hold() {
	var area = [];
	area.push({shape: "rect", key: "hold_off_off", coords: "156,336,319,364", hint_text: "Hold Off Off"});
	area.push({shape: "rect", key: "set_trigger_hold_off", coords: "420,336,581,363", hint_text: "Trigger hold off 200us"});
	return area;
}
function menu_set_stat() {
	var area = [];
	area.push({shape: "rect", key: "stat_sync_delay", coords: "108,336,241,364", hint_text: "No. Of Samples 100"});
	area.push({shape: "rect", key: "stat_start_delay", coords: "243,336,373,363", hint_text: "No. Of Bins 20"});
	area.push({shape: "rect", key: "stat_burst_meas_time", coords: "375,336,515,363", hint_text: "Pacing Off"});
	area.push({shape: "rect", key: "stat_freq_limit", coords: "517,336,645,363", hint_text: "Pacing Time 1s"});
	return area;
}
function menu_set_timebase() {
	var area = [];
	area.push({shape: "rect", key: "timebase_int", coords: "145,334,239,362", hint_text: "Int"});
	area.push({shape: "rect", key: "timebase_ext", coords: "309,335,395,362", hint_text: "Ext"});
	area.push({shape: "rect", key: "timebase_auto", coords: "461,336,562,363", hint_text: "Auto"});
	return area;
}
function menu_set_misc() {
	var area = [];
	area.push({shape: "rect", key: "misc_sync_delay", coords: "108,336,241,364", hint_text: "Smart Time Interval Off"});
	area.push({shape: "rect", key: "misc_start_delay", coords: "243,336,373,363", hint_text: "Smart Freq (Auto)"});
	area.push({shape: "rect", key: "misc_burst_meas_time", coords: "375,336,515,363", hint_text: "Auto Trig Low Freq 100Hz"});
	area.push({shape: "rect", key: "misc_freq_limit", coords: "517,336,645,363", hint_text: "Timeout (Off)"});
	return area;
}

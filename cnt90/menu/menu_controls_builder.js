function MenuControlsBuilder() {
  this.map = null;
}

MenuControlsBuilder.prototype.setMap = function (map) {
  this.map = map;
}

//Meas function

MenuControlsBuilder.prototype.menu_meas_func_controls = function () {
  meas_func_controls = [];
	meas_func_controls.push({type: "menu", key: 'Freq', cls: MenuElement, param: {cond: 'state.power', act: '', val: '"Freq"'}});
	meas_func_controls.push({type: "menu", key: 'Time', cls: MenuElement, param: {cond: 'state.power', act: '', val: '"Time"'}});
	meas_func_controls.push({type: "menu", key: 'Phase', cls: MenuElement, param: {cond: 'state.power', act: '', val: '"Phase"'}});
	meas_func_controls.push({type: "menu", key: 'Volt', cls: MenuElement, param: {cond: 'state.power', act: '', val: '"Volt"'}});

  return meas_func_controls;
}
// Phase

MenuControlsBuilder.prototype.menu_meas_func_phase_controls = function () {
	menu_area = [];
	menu_area.push({type: 'menu', shape: "rect", key: "A_and_B", cls: MenuElement, param: {cond: 'state.power', act: '', val: '"Phase A & B"'}});
	menu_area.push({type: 'menu', shape: "rect", key: "A_and_C", cls: MenuElement, param: {cond: 'state.power', act: '', val: '"Phase A & C"'}});
	menu_area.push({type: 'menu', shape: "rect", key: "B_and_C", cls: MenuElement, param: {cond: 'state.power', act: '', val: '"Phase B & C"'}});
	return menu_area;
}

//Frequentcy

MenuControlsBuilder.prototype.menu_meas_func_freq_controls = function () {
	menu_area = [];
	menu_area.push({type: 'menu', shape: "rect", key: "meas_func_freq", cls: MenuElement, param: {cond: 'state.power', act: '', val: '"Freq"'}});
	menu_area.push({type: 'menu', shape: "rect", key: "meas_func_freq_rat", cls: MenuElement, param: {cond: 'state.power', act: '', val: '"Freq Ratio"'}});
	menu_area.push({type: 'menu', shape: "rect", key: "meas_func_brust", cls: MenuElement, param: {cond: 'state.power', act: '', val: '"Brust"'}});
	return menu_area;
}

MenuControlsBuilder.prototype.menu_meas_func_freq_freq_controls = function () {
	menu_area = [];
	menu_area.push({type: 'menu', shape: "rect", key: "meas_func_freq_input_a", cls: MenuElement, param: {cond: 'state.power', act: '', val: '"Input A"'}});
	menu_area.push({type: 'menu', shape: "rect", key: "meas_func_freq_input_b", cls: MenuElement, param: {cond: 'state.power', act: '', val: '"Input B"'}});
	menu_area.push({type: 'menu', shape: "rect", key: "meas_func_freq_input_c", cls: MenuElement, param: {cond: 'state.power', act: '', val: '"Input C"'}});
	return menu_area;
}

MenuControlsBuilder.prototype.menu_freq_ratio_controls = function() {
	var area = [];
	area.push({type: 'menu', key: "A/B", cls: MenuElement, param: {cond: 'state.power', act: '', val: '"A/B"'}});
	area.push({type: 'menu', key: "B/A", cls: MenuElement, param: {cond: 'state.power', act: '', val: '"B/A"'}});
	area.push({type: 'menu', key: "C/A", cls: MenuElement, param: {cond: 'state.power', act: '', val: '"C/A"'}});
	area.push({type: 'menu', key: "C/B", cls: MenuElement, param: {cond: 'state.power', act: '', val: '"C/B"'}});
	return area;
}
// Volt

MenuControlsBuilder.prototype.menu_meas_func_volt_controls = function () {
	menu_area = [];
	menu_area.push({type: 'menu', shape: "rect", key: "meas_func_volt_input_a", cls: MenuElement, param: {cond: 'state.power', act: '', val: '"Input A"'}});
	menu_area.push({type: 'menu', shape: "rect", key: "meas_func_volt_input_b", cls: MenuElement, param: {cond: 'state.power', act: '', val: '"Input B"'}});
	menu_area.push({type: 'menu', shape: "rect", key: "meas_func_volt_input_c", cls: MenuElement, param: {cond: 'state.power', act: '', val: '"Input C"'}});
	return menu_area;
}

//Time

MenuControlsBuilder.prototype.menu_meas_func_time_controls = function () {
	menu_area = [];
	menu_area.push({type: 'menu', shape: "rect", key: "meas_func_time_interval", cls: MenuElement, param: {cond: 'state.power', act: '', val: '"Time interval"'}});
	menu_area.push({type: 'menu', shape: "rect", key: "meas_func_rise_fall_time", cls: MenuElement, param: {cond: 'state.power', act: '', val: '"Rise/Fall time"'}});
	menu_area.push({type: 'menu', shape: "rect", key: "meas_func_pulse_width", cls: MenuElement, param: {cond: 'state.power', act: '', val: '"Pulse widthBrust"'}});
	menu_area.push({type: 'menu', shape: "rect", key: "meas_func_duty", cls: MenuElement, param: {cond: 'state.power', act: '', val: '"Duty"'}});	
	return menu_area;
}

//Time Interval

MenuControlsBuilder.prototype.time_interval_controls = function () {
	menu_area = [];
	menu_area.push({type: 'menu', shape: "rect", key: "IntervalAtoB", cls: MenuElement, param: {cond: 'state.power', act: '', val: '"Interval A to B"'}});
	menu_area.push({type: 'menu', shape: "rect", key: "IntervalBtoA", cls: MenuElement, param: {cond: 'state.power', act: '', val: '"Interval B to A"'}});
	//this.set_fonts(menu_area);
	return menu_area;
}

//rise fall time

MenuControlsBuilder.prototype.rise_fall_time_controls = function() {
	var area = [];
	area.push({type: 'menu', shape: "rect", key: "rise_input_a", cls: MenuElement, param: {cond: 'state.power', act: '', val: '"Rise input A"'}});
	area.push({type: 'menu', shape: "rect", key: "fall_input_a", cls: MenuElement, param: {cond: 'state.power', act: '', val: '"Fall input A"'}});
	area.push({type: 'menu', shape: "rect", key: "rise_input_b", cls: MenuElement, param: {cond: 'state.power', act: '', val: '"Rise input B"'}});
	area.push({type: 'menu', shape: "rect", key: "fall_input_b", cls: MenuElement, param: {cond: 'state.power', act: '', val: '"Fall input B"'}});
	//this.set_fonts(area);
	return area;
}

// pulse width

MenuControlsBuilder.prototype.pulse_width_controls = function () {
	menu_area = [];
	menu_area.push({type: 'menu', shape: "rect", key: "pulse_width_a", cls: MenuElement, param: {cond: 'state.power', act: '', val: '"Pulse width A"'}});
	menu_area.push({type: 'menu', shape: "rect", key: "pulse_width_b", cls: MenuElement, param: {cond: 'state.power', act: '', val: '"Pulse width B"'}});
	//this.set_fonts(menu_area);
	return menu_area;
}

// Duty

MenuControlsBuilder.prototype.duty_controls = function () {
	menu_area = [];
	menu_area.push({type: 'menu', shape: "rect", key: "duty_a", cls: MenuElement, param: {cond: 'state.power', act: '', val: '"Duty A"'}});
	menu_area.push({type: 'menu', shape: "rect", key: "duty_b", cls: MenuElement, param: {cond: 'state.power', act: '', val: '"Duty B"'}});
	//this.set_fonts(menu_area);
	return menu_area;
}
/*
MenuControlsBuilder.prototype.menu_meas_func_phase_controls = function () {
	menu_area = [];
	menu_area.push({type: 'menu', shape: "rect", key: "meas_func_phase", cls: MenuElement, param: {cond: 'state.power', act: '', val: '"Time interval"'}});	
	return menu_area;
}
*/

MenuControlsBuilder.prototype.menu_statplot_numerical_controls = function() {
	var menu_statplot_numerical_controls = [];
	menu_statplot_numerical_controls.push({type: "menu", key: "stat_plot_mean_label", cls: MenuElement, param: {cond: 'state.power', act: '', val: '"N:"'}});
	menu_statplot_numerical_controls.push({type: "menu", key: "stat_plot_mean", cls: MenuElement, param: {cond: 'state.power', act: '', val: 'cnt90.state.stat_plot.N'}});
	menu_statplot_numerical_controls.push({type: "menu", key: "stat_plot_max_label", cls: MenuElement, param: {cond: 'state.power', act: '', val: '"Max:"'}});
	menu_statplot_numerical_controls.push({type: "menu", key: "stat_plot_max", cls: MenuElement, param: {cond: 'state.power', act: '', val: 'cnt90.state.stat_plot.MaxPM'}});

	menu_statplot_numerical_controls.push({type: "menu", key: "stat_plot_min_label", cls: MenuElement, param: {cond: 'state.power', act: '', val: '"Min:"'}});
	menu_statplot_numerical_controls.push({type: "menu", key: "stat_plot_min", cls: MenuElement, param: {cond: 'state.power', act: '', val: 'cnt90.state.stat_plot.MinPM'}});

	menu_statplot_numerical_controls.push({type: "menu", key: "stat_plot_p_p_label", cls: MenuElement, param: {cond: 'state.power', act: '', val: '"P-P:"'}});
	menu_statplot_numerical_controls.push({type: "menu", key: "stat_plot_p_p", cls: MenuElement, param: {cond: 'state.power', act: '', val: 'cnt90.state.stat_plot.P_PPM'}});

	menu_statplot_numerical_controls.push({type: "menu", key: "stat_plot_adev_label", cls: MenuElement, param: {cond: 'state.power', act: '', val: '"Adev:"'}});
	menu_statplot_numerical_controls.push({type: "menu", key: "stat_plot_adev", cls: MenuElement, param: {cond: 'state.power', act: '', val: 'cnt90.state.stat_plot.AdevPM'}});

	menu_statplot_numerical_controls.push({type: "menu", key: "stat_plot_std_label", cls: MenuElement, param: {cond: 'state.power', act: '', val: '"Std:"'}});
	menu_statplot_numerical_controls.push({type: "menu", key: "stat_plot_std", cls: MenuElement, param: {cond: 'state.power', act: '', val: 'cnt90.state.stat_plot.StdPM'}});
	
	return menu_statplot_numerical_controls;
}

MenuControlsBuilder.prototype.menu_statplot_trendplot_controls = function() {
	var menu_statplot_trendplot_controls = [];
	menu_statplot_trendplot_controls.push({type: "menu", key: "trend", cls: MenuElement, param: {cond: 'state.power', act: '', val: '"trend"'}});
	menu_statplot_trendplot_controls.push({type: "menu", key: "upper_lim", cls: MenuElement, param: {cond: 'state.power', act: '', val: '"upper_lim"'}});
	menu_statplot_trendplot_controls.push({type: "menu", key: "lower_lim", cls: MenuElement, param: {cond: 'state.power', act: '', val: '"lower_lim"'}});
	menu_statplot_trendplot_controls.push({type: "menu", key: "measure", cls: MenuElement, param: {cond: 'state.power', act: '', val: '"measure"'}});
	menu_statplot_trendplot_controls.push({type: "menu", key: "time", cls: MenuElement, param: {cond: 'state.power', act: '', val: '"time"'}});
	return menu_statplot_trendplot_controls;
}

MenuControlsBuilder.prototype.menu_settings_controls = function () {
	menu_settings_controls = [];
	menu_settings_controls.push({type: "menu", key: 'meas_time', cls: MenuElement, param: {cond: 'state.power', act: '', val: '"MeasTime 10ms"'}});
	menu_settings_controls.push({type: "menu", key: 'burst', cls: MenuElement, param: {cond: 'state.power', act: '', val: '"Burst"'}});
	menu_settings_controls.push({type: "menu", key: 'arm', cls: MenuElement, param: {cond: 'state.power', act: '', val: '"Arm"'}});
	menu_settings_controls.push({type: "menu", key: 'trigger_hold_off', cls: MenuElement, param: {cond: 'state.power', act: '', val: '"Trigger Hold Off (Off)"'}});
	menu_settings_controls.push({type: "menu", key: 'stat', cls: MenuElement, param: {cond: 'state.power', act: '', val: '"Stat"'}});
	menu_settings_controls.push({type: "menu", key: 'timebase_ref', cls: MenuElement, param: {cond: 'state.power', act: '', val: '"Timebase Ref (Int)"'}});
	menu_settings_controls.push({type: "menu", key: 'misc', cls: MenuElement, param: {cond: 'state.power', act: '', val: '"Misc"'}});
	return menu_settings_controls;
}

MenuControlsBuilder.prototype.menu_input_controls = function () {
	menu_input_controls = [];
	menu_input_controls.push({type: "menu", key: 'drop_minus', cls: MenuElement, param: {cond: 'state.power', act: '', val: '"|_"'}});
	menu_input_controls.push({type: "menu", key: 'drop_plus', cls: MenuElement, param: {cond: 'state.power', act: '', val: '"_|"'}});
	menu_input_controls.push({type: "menu", key: 'dc', cls: MenuElement, param: {cond: 'state.power', act: '', val: '"DC"'}});
	menu_input_controls.push({type: "menu", key: 'ac', cls: MenuElement, param: {cond: 'state.power', act: '', val: '"AC"'}});
	menu_input_controls.push({type: "menu", key: 'R1', cls: MenuElement, param: {cond: 'state.power', act: '', val: '"1 МОм"'}});
	menu_input_controls.push({type: "menu", key: 'R50', cls: MenuElement, param: {cond: 'state.power', act: '', val: '"50 Ом"'}});
	menu_input_controls.push({type: "menu", key: 'fading10', cls: MenuElement, param: {cond: 'state.power', act: '', val: '"10x"'}});
	menu_input_controls.push({type: "menu", key: 'fading1', cls: MenuElement, param: {cond: 'state.power', act: '', val: '"1x"'}});
	menu_input_controls.push({type: "menu", key: 'start_auto', cls: MenuElement, param: {cond: 'state.power', act: '', val: '"Auto"'}});
	menu_input_controls.push({type: "menu", key: 'start_man', cls: MenuElement, param: {cond: 'state.power', act: '', val: '"Man"'}});
	menu_input_controls.push({type: "menu", key: 'trig_input', cls: MenuElement, param: {cond: 'state.power', act: '', val: '"Trig 0 V"'}});
	menu_input_controls.push({type: "menu", key: 'filter_input', cls: MenuElement, param: {cond: 'state.power', act: '', val: '"Filter (Off)"'}});
	return menu_input_controls;
}

MenuControlsBuilder.prototype.input_filter_controls = function () {
	var input_filter_controls = [];
	input_filter_controls.push({type: "menu", key: "an_lp", cls: MenuElement, param: {cond: 'state.power', act: '', val: '"Analog LP 100kHz Off"'}});
	input_filter_controls.push({type: "menu", key: "dig_lp", cls: MenuElement, param: {cond: 'state.power', act: '', val: '"Digital LP Off"'}});
	input_filter_controls.push({type: "menu", key: "dig_lp_freq", cls: MenuElement, param: {cond: 'state.power', act: '', val: '"Digital LP Freq 100kHz"'}});
	return input_filter_controls;
}

MenuControlsBuilder.prototype.lp_val_controls = function () {
	var lp_val_controls = [];
	lp_val_controls.push({type: "menu", key: "name_lp", cls: MenuElement, param: {cond: 'state.power', act: '', val: '"Analog LP 100kHz Off"'}});
	lp_val_controls.push({type: "menu", key: "conut_hz", cls: MenuElement, param: {cond: 'state.power', act: '', val: '"0 Hz"'}});
	lp_val_controls.push({type: "menu", key: "lp_hz", cls: MenuElement, param: {cond: 'state.power', act: '', val: '"Hz"'}});
	lp_val_controls.push({type: "menu", key: "lp_hz", cls: MenuElement, param: {cond: 'state.power', act: '', val: '"MHz"'}});
	return lp_val_controls;
}

MenuControlsBuilder.prototype.menu_set_burst_controls = function () {
	var menu_set_burst_controls = [];
	menu_set_burst_controls.push({type: "menu", key: "burst_sync_delay", cls: MenuElement, param: {cond: 'state.power', act: '', val: '"Sync delay 200us"'}});
	menu_set_burst_controls.push({type: "menu", key: "burst_start_delay", cls: MenuElement, param: {cond: 'state.power', act: '', val: '"Start delay 200us"'}});
	menu_set_burst_controls.push({type: "menu", key: "burst_meas_time", cls: MenuElement, param: {cond: 'state.power', act: '', val: '"Meas Time 200us"'}});
	menu_set_burst_controls.push({type: "menu", key: "burst_freq_limit", cls: MenuElement, param: {cond: 'state.power', act: '', val: '"Frequency Limit 300MHz"'}});
	return menu_set_burst_controls;
}

MenuControlsBuilder.prototype.menu_set_arm_controls = function () {
	var menu_set_arm_controls = [];
	menu_set_arm_controls.push({type: "menu", key: "arm_start_chan_off", cls: MenuElement, param: {cond: 'state.power', act: '', val: '"Start Chan Off"'}});
	menu_set_arm_controls.push({type: "menu", key: "arm_start_slope", cls: MenuElement, param: {cond: 'state.power', act: '', val: '"Start Slope"'}});
	menu_set_arm_controls.push({type: "menu", key: "arm_start_arm_delay", cls: MenuElement, param: {cond: 'state.power', act: '', val: '"Start Arm delay 200us"'}});
	menu_set_arm_controls.push({type: "menu", key: "arm_stop_chan_off", cls: MenuElement, param: {cond: 'state.power', act: '', val: '"Stop Chan Off"'}});
	menu_set_arm_controls.push({type: "menu", key: "arm_stop_slope", cls: MenuElement, param: {cond: 'state.power', act: '', val: '"Stop Slope"'}});
	return menu_set_arm_controls;
}

MenuControlsBuilder.prototype.menu_set_trigger_hold_controls = function () {
	var menu_set_trigger_hold_controls = [];
	menu_set_trigger_hold_controls.push({type: "menu", key: "hold_off_off", cls: MenuElement, param: {cond: 'state.power', act: '', val: '"Hold Off Off"'}});
	menu_set_trigger_hold_controls.push({type: "menu", key: "set_trigger_hold_off", cls: MenuElement, param: {cond: 'state.power', act: '', val: '"Trigger hold off 200us"'}});
	return menu_set_trigger_hold_controls;
}

MenuControlsBuilder.prototype.menu_set_stat_controls = function () {
	var menu_set_stat_controls = [];
	menu_set_stat_controls.push({type: "menu", key: "stat_sync_delay", cls: MenuElement, param: {cond: 'state.power', act: '', val: '"No. Of Samples 100"'}});
	menu_set_stat_controls.push({type: "menu", key: "stat_start_delay", cls: MenuElement, param: {cond: 'state.power', act: '', val: '"No. Of Bins 20"'}});
	menu_set_stat_controls.push({type: "menu", key: "stat_burst_meas_time", cls: MenuElement, param: {cond: 'state.power', act: '', val: '"Pacing Off"'}});
	menu_set_stat_controls.push({type: "menu", key: "stat_freq_limit", cls: MenuElement, param: {cond: 'state.power', act: '', val: '"Pacing Time 1s"'}});
	return menu_set_stat_controls;
}

MenuControlsBuilder.prototype.menu_set_timebase_controls = function () {
	var menu_set_timebase_controls = [];
	menu_set_timebase_controls.push({type: "menu", key: "timebase_int", cls: MenuElement, param: {cond: 'state.power', act: '', val: '"Int"'}});
	menu_set_timebase_controls.push({type: "menu", key: "timebase_ext", cls: MenuElement, param: {cond: 'state.power', act: '', val: '"Ext"'}});
	menu_set_timebase_controls.push({type: "menu", key: "timebase_auto", cls: MenuElement, param: {cond: 'state.power', act: '', val: '"Auto"'}});
	return menu_set_timebase_controls;
}

MenuControlsBuilder.prototype.menu_set_misc_controls = function () {
	var menu_set_misc_controls = [];
	menu_set_misc_controls.push({type: "menu", key: "misc_sync_delay", cls: MenuElement, param: {cond: 'state.power', act: '', val: '"Smart Time Interval Off"'}});
	menu_set_misc_controls.push({type: "menu", key: "misc_start_delay", cls: MenuElement, param: {cond: 'state.power', act: '', val: '"Smart Freq (Auto)"'}});
	menu_set_misc_controls.push({type: "menu", key: "misc_burst_meas_time", cls: MenuElement, param: {cond: 'state.power', act: '', val: '"Auto Trig Low Freq 100Hz"'}});
	menu_set_misc_controls.push({type: "menu", key: "misc_freq_limit", cls: MenuElement, param: {cond: 'state.power', act: '', val: '"Timeout (Off)"'}});
	return menu_set_misc_controls;
}

MenuControlsBuilder.prototype.menu_math_limit_controls = function () {
	var menu_math_limit_controls = [];
	menu_math_limit_controls.push({type: "menu", key: "menu_math", cls: MenuElement, param: {cond: 'state.power', act: '', val: '"Math"'}});
	menu_math_limit_controls.push({type: "menu", key: "menu_limit", cls: MenuElement, param: {cond: 'state.power', act: '', val: '"Limits"'}});
	return menu_math_limit_controls;
}

MenuControlsBuilder.prototype.math_constant_controls = function () {
	var area = [];
  index_of_limit = this.map.menu_bar.menu_elements_configuration[this.map.menu_bar.current_pos_way][this.map.menu_bar.current_index]['previous_menu_position'];
  prev_menu = this.map.menu_bar.findPreviousMenu();
  type_of_limit = this.map.menu_bar.menu_elements_configuration[prev_menu][index_of_limit].key;
  type_of_limit = type_of_limit.substring(0, type_of_limit.length - 4) + "erLimit";
	area.push({type: "menu", key: "constant", cls: MenuElement, param: {cond: 'state.power', act: '', val: '"K:"'}});
	area.push({type: "menu", key: "count_mantis", cls: MenuElement, param: {cond: 'state.power && state.limits.indLimShow == 0', act: '', val: 'state.limits.' + type_of_limit + 'Mantissa'}});
	area.push({type: "menu", key: "const_ee", cls: MenuElement, param: {cond: 'state.power', act: '', val:  '"EE:"'}});
	area.push({type: "menu", key: "count_exp", cls: MenuElement, param: {cond: 'state.power && state.limits.indLimShow == 0', act: '', val: 'state.limits.' + type_of_limit + 'Count'}});
	area.push({type: "menu", key: "menu_ee", cls: MenuElement, param: {cond: 'state.power', act: '', val: '"EE"'}});
	area.push({type: "menu", key: "menu_const_ok", cls: MenuElement, param: {cond: 'state.power', act: '', val: '"X0"'}});
	return area;
}

MenuControlsBuilder.prototype.math_controls = function () {
	var math_controls = [];
	math_controls.push({type: "menu", key: "math_math", cls: MenuElement, param: {cond: 'state.power', act: '', val: '"Math (Off)"'}});
	math_controls.push({type: "menu", key: "math_k", cls: MenuElement, param: {cond: 'state.power', act: '', val: '"K 1 E0"'}});
	math_controls.push({type: "menu", key: "math_l", cls: MenuElement, param: {cond: 'state.power', act: '', val: '"L 0 E0"'}});
	math_controls.push({type: "menu", key: "math_m", cls: MenuElement, param: {cond: 'state.power', act: '', val: '"M 1 E0"'}});
	return math_controls;
}

MenuControlsBuilder.prototype.math_math_controls = function () {
	var math_math_controls = [];
	math_math_controls.push({type: "menu", key: "math_math_off", cls: MenuElement, param: {cond: 'state.power', act: '', val: '"Off"'}});
	math_math_controls.push({type: "menu", key: "math_func1", cls: MenuElement, param: {cond: 'state.power', act: '', val: '"K*X+L"'}});
	math_math_controls.push({type: "menu", key: "math_func2", cls: MenuElement, param: {cond: 'state.power', act: '', val: '"K/X+L"'}});
	math_math_controls.push({type: "menu", key: "math_func3", cls: MenuElement, param: {cond: 'state.power', act: '', val: '"K*X/M+L"'}});
	math_math_controls.push({type: "menu", key: "math_func4", cls: MenuElement, param: {cond: 'state.power', act: '', val: '"(K/X+L)/M"'}});
	return math_math_controls;
}

MenuControlsBuilder.prototype.limits_controls = function () {
	var limit_controls = [];
  behaviout = this.map.state.limits.state;
  mode = this.map.state.limits.mode;
	limit_controls.push({type: "menu", key: "limit_beh", cls: MenuElement, param: {cond: 'state.power', act: '', val: '"Limit Behavior (" + this.map.state.limits.state + ")"'}});
	limit_controls.push({type: "menu", key: "limit_mode", cls: MenuElement, param: {cond: 'state.power', act: '', val: '"Limit Mode (" + this.map.state.limits.mode + ")"'}});
	limit_controls.push({type: "menu", key: "low_lim", cls: MenuElement, param: {cond: 'state.power', act: '', val: '"Lower Limit " + this.map.state.limits.lowerLimitMantissa + " E " + this.map.state.limits.lowerLimitCount'}});
	limit_controls.push({type: "menu", key: "upp_lim", cls: MenuElement, param: {cond: 'state.power', act: '', val: '"Upper Limit " + this.map.state.limits.upperLimitMantissa + " E " + this.map.state.limits.upperLimitCount'}});
	return limit_controls;
}

MenuControlsBuilder.prototype.limits_state_controls = function () {
    var limit_behavior_controls = [];
    limit_behavior_controls.push({ type: "menu", key: "limit_beh_off", cls: MenuElement, param: { cond: 'state.power', act: '', val: '"Off"' } });
    limit_behavior_controls.push({ type: "menu", key: "limit_beh_capture", cls: MenuElement, param: { cond: 'state.power', act: '', val: '"Capture"' } });
    limit_behavior_controls.push({ type: "menu", key: "limit_beh_alarm", cls: MenuElement, param: { cond: 'state.power', act: '', val: '"Alarm"' } });
    limit_behavior_controls.push({ type: "menu", key: "limit_beh_alarm_stop", cls: MenuElement, param: { cond: 'state.power', act: '', val: '"Alarm stop"' } });
    return limit_behavior_controls;
}

MenuControlsBuilder.prototype.limits_mode_controls = function () {
    var limit_mode_controls = [];
    limit_mode_controls.push({ type: "menu", key: "limit_mode_above", cls: MenuElement, param: { cond: 'state.power', act: '', val: '"Above"' } });
    limit_mode_controls.push({ type: "menu", key: "limit_mode_below", cls: MenuElement, param: { cond: 'state.power', act: '', val: '"Below"' } });
    limit_mode_controls.push({ type: "menu", key: "limit_mode_range", cls: MenuElement, param: { cond: 'state.power', act: '', val: '"Range"' } });
    return limit_mode_controls;
}

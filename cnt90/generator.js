CNT90.Generator = function (name, param) {
    CNT90.Generator.superclass.constructor.call(this, name, param);
    this.current_pos_way = '';
    this.flagTriger1 = true;
    this.flagTriger2 = true;
    var startLeadingFront = 0;
    var stopLeadingFront = 0;
    var startTrailingFront = 0;
    var stopTrailingFront = 0;
    this.VdPrev = 0;
    this.VdNext = 0;
}

extend(CNT90.Generator, Logic);

CNT90.Generator.prototype.setMap = function (map) {
    CNT90.Generator.superclass.setMap.call(this, map);
    var g = this;
    var state = this.map.state;
    map.addListener('state_change', this, function () {
        console.log('state_chage');
        console.log(map.state);
    });

    map.addListener('button_click', this, function (c) {
        //console.log('button_click');
        //console.log(c);
    });

    map.addListener('button_post', this, function (c) {
        //console.log('button_post'); 		//console.log(c);
    });
}

CNT90.Generator.prototype.input = function (value) {
    var state = this.map.state;
    state.value = value;
    this.map.action(this, 'state_change');
}

CNT90.Generator.prototype.off = function () {
    var state = this.map.state;
    state.power = false;
    this.map.action(this, 'state_change');
}



// вызывается при нажатие на кнопку Value, в зависимости от выбранного пункта меню выводит на экран частоту
CNT90.Generator.prototype.actionSignal = function () {
    var selected_item = this.map.state.measurement;//this.map.menu_bar.menu_elements_configuration[this.map.menu_bar.current_pos_way][this.map.menu_bar.current_index].key;
    switch (selected_item) {
        case "meas_func_freq_input_a":
            this.map.state.meas_ment = "Hz";
            this.map.state.presintationModeOfValue = this.map.transferSystem(this.map.state.signalA.freq);
            this.map.state.text = this.map.state.signalA.freq;
            break;
        case "meas_func_freq_input_b":
            this.map.state.meas_ment = "Hz";
            this.map.state.presintationModeOfValue = this.map.transferSystem(this.map.state.signalB.freq);
            this.map.state.text = this.map.state.signalB.freq;
            break;
        case "meas_func_freq_input_c":
            this.map.state.meas_ment = "Hz";
            this.map.state.presintationModeOfValue = this.map.transferSystem(this.map.state.signalC.freq);
            this.map.state.text = this.map.state.signalC.freq;
            break;
        case "meas_func_volt_input_a":
            this.map.state.meas_ment = "V";
            this.map.state.presintationModeOfValue = this.map.transferSystem(this.map.state.signalA.v);
            this.map.state.text = this.map.state.signalA.v;
            break;
        case "meas_func_volt_input_b":
            this.map.state.meas_ment = "V";
            this.map.state.presintationModeOfValue = this.map.transferSystem(this.map.state.signalB.v);
            this.map.state.text = this.map.state.signalB.v;
            break;
        case "meas_func_volt_input_c":
            this.map.state.meas_ment = "V";
            this.map.state.presintationModeOfValue = this.map.transferSystem(this.map.state.signalC.v);
            this.map.state.text = this.map.state.signalC.v;
            break;
        case "A/B":
            this.map.state.meas_ment = "";
            this.map.state.text = this.map.state.signalA.freq / this.map.state.signalB.freq;
            this.map.state.presintationModeOfValue = this.map.state.text;
            break;
        case "B/A":
            this.map.state.meas_ment = "";
            this.map.state.text = this.map.state.signalB.freq / this.map.state.signalA.freq;
            this.map.state.presintationModeOfValue = this.map.state.text;
            break;
        case "C/A":
            this.map.state.meas_ment = "";
            this.map.state.text = this.map.state.signalC.freq / this.map.state.signalA.freq;
            this.map.state.presintationModeOfValue = this.map.state.text;
            break;
        case "C/B":
            this.map.state.meas_ment = "";
            this.map.state.text = this.map.state.signalC.freq / this.map.state.signalB.freq;
            this.map.state.presintationModeOfValue = this.map.state.text;
            break;
        case "IntervalAtoB":
            this.map.state.meas_ment = "s";
            this.map.state.text = this.map.state.timeInterval.AtoB;
            this.map.state.presintationModeOfValue = this.map.state.text;
            break;
        case "IntervalBtoA":
            this.map.state.meas_ment = "s";
            this.map.state.text = this.map.state.timeInterval.BtoA;
            this.map.state.presintationModeOfValue = this.map.state.text;
            break;
        case "rise_input_a":
            this.map.state.meas_ment = "s";
            this.map.state.text = this.map.state.signalA.rise;
            this.map.state.presintationModeOfValue = this.map.state.text;
            break;
        case "fall_input_a":
            this.map.state.meas_ment = "s";
            this.map.state.text = this.map.state.signalA.fall;
            this.map.state.presintationModeOfValue = this.map.state.text;
            break;
        case "rise_input_b":
            this.map.state.meas_ment = "s";
            this.map.state.text = this.map.state.signalB.rise;
            this.map.state.presintationModeOfValue = this.map.state.text;
            break;
        case "fall_input_b":
            this.map.state.meas_ment = "s";
            this.map.state.text = this.map.state.signalB.fall;
            this.map.state.presintationModeOfValue = this.map.state.text;
            break;
        case "pulse_width_a":
            this.map.state.meas_ment = "s";
            this.map.state.text = this.map.state.signalA.pulseW;
            this.map.state.presintationModeOfValue = this.map.state.text;
            break;
        case "pulse_width_b":
            this.map.state.meas_ment = "s";
            this.map.state.text = this.map.state.signalB.pulseW;
            this.map.state.presintationModeOfValue = this.map.state.text;
            break;
        case "duty_a":
            this.map.state.meas_ment = "";
            this.map.state.text = this.map.state.signalA.duty;
            this.map.state.presintationModeOfValue = this.map.state.text;
            break;
        case "duty_b":
            this.map.state.meas_ment = "";
            this.map.state.text = this.map.state.signalB.duty;
            this.map.state.presintationModeOfValue = this.map.state.text;
            break;
        case "A_and_B":
            this.map.state.meas_ment = "°";
            this.map.state.text = this.map.state.phase.AandB;
            this.map.state.presintationModeOfValue = this.map.state.text;
            break;
        case "A_and_C":
            this.map.state.meas_ment = "°";
            this.map.state.text = this.map.state.phase.AandC;
            this.map.state.presintationModeOfValue = this.map.state.text;
            break;
        case "B_and_C":
            this.map.state.meas_ment = "°";
            this.map.state.text = this.map.state.phase.BandC;
            this.map.state.presintationModeOfValue = this.map.state.text;
            break;
    };
}
CNT90.Generator.prototype.getLevels = function (amp, funcType) {
    switch (funcType) {
        case "sin":
            {
                return levels = { 'lowLevel': -0.9 * amp, 'highLevel': 0.9 * amp };
            }
        case "impulse":
            {
                return levels = { 'lowLevel': 0.1 * amp, 'highLevel': 0.9 * amp };
            }
    }
    return levels = { 'lowLevel': undefined, 'highLevel': undefined };
}
CNT90.Generator.prototype.getValueWithTime = function (amp, freq, time, funcType, offset, duration) {

    switch (funcType) {
        case "sin":
            {
                return amp * Math.sin(2 * 3.14 * freq * (time + offset));
            }
        case "impulse":
            {
                var period = 1 / freq;
                var numOfCycles = parseInt(time / period);
                var timeOfLastPeriod = time - period * numOfCycles;                
                if (timeOfLastPeriod < duration) {
                    return amp;
                } else {
                    return 0;
                }
            }
    }
}

CNT90.Generator.prototype.riseFallTime = function (amp, freq, offset, funcType, duration) {


    var levels = this.getLevels(amp, funcType);

    // startLeadingFront - время стартового состояния на входе (передний фронт)
    // stopLeadingFront - время стопового состояния на входе (передний фронт)
    // startTrailingFront - время стартового состояния на входе (следующий задний фронт)
    // stopTrailingFront - время стопового состояния на входе (следующий задний фронт)
    // stopLeadingFront - startLeadingFront = rise time 
    // stopTrailingFront - startTrailingFront = fall time
    var startLeadingFront = undefined;
    var stopLeadingFront = undefined;
    var startTrailingFront = undefined;
    var stopTrailingFront = undefined;
    var time = 0;
    var deltaTime = 1 / (freq * 1000);
    var dotType = "startLeadingFront";
    while (startLeadingFront == undefined || stopLeadingFront == undefined || startTrailingFront == undefined || stopTrailingFront == undefined) {
        this.VdPrev = this.getValueWithTime(amp, freq, time - deltaTime, funcType, offset, duration);
        var VdCur = this.getValueWithTime(amp, freq, time, funcType, offset, duration);
        this.VdNext = this.getValueWithTime(amp, freq, time + deltaTime, funcType, offset, duration);
        if (levels.lowLevel > this.VdPrev && levels.lowLevel <= VdCur && dotType == "startLeadingFront") {
            startLeadingFront = time;
            dotType = "stopLeadingFront";
        }

        if (levels.highLevel > this.VdPrev && levels.highLevel <= VdCur && dotType == "stopLeadingFront") {
            stopLeadingFront = time;
            dotType = "startTrailingFront";
        }

        if (levels.highLevel <= this.VdPrev && levels.highLevel >= VdCur && dotType == "startTrailingFront") {
            startTrailingFront = time;
            dotType = "stopTrailingFront";
        }

        if (levels.lowLevel <= this.VdPrev && levels.lowLevel >= VdCur && dotType == "stopTrailingFront") {
            stopTrailingFront = time;
        }
        time += deltaTime;
    }
    //if (this.stopLeadingFront - this.startLeadingFront > 0 && this.t4 - this.startTrailingFront > 0) {
    var func = [startLeadingFront, stopLeadingFront, startTrailingFront, stopTrailingFront];
    //} else var func = [0, 0, 0, 0];
    //console.log(cnt90.timer, this.startLeadingFront, this.stopLeadingFront, this.startTrailingFront, this.stopTrailingFront);    
    return func;
}

CNT90.Generator.prototype.timeAndPhaseMenu = function (type, v, freq, offset, duration) {
    //if (this.map.menu_bar.current_pos_way != "menu_statplot_numerical") {
    //  this.current_pos_way = this.map.menu_bar.current_pos_way;
    //}
    if (this.current_pos_way == "time_interval") {
        var func = this.map.generator.riseFallTime(v, freq, offset, type, duration);
    }
    if (this.current_pos_way == "rise_fall_time") {
        var x = this.map.generator.riseFallTime(v, freq, offset, type, duration);
        var T1 = x[1] - x[0]; //rise
        var T2 = x[3] - x[2]; //fall
        var func = [T1, T2];
    }
    if (this.current_pos_way == "pulse_width") {
        var x = this.map.generator.riseFallTime(v, freq, offset, type, duration);
        var func = x[2] - x[1];
    }
    if (this.current_pos_way == "duty") {
        var x = this.map.generator.riseFallTime(v, freq, offset, type, duration);
        var func = (x[2] - x[1]) * freq;
    }
    if (this.current_pos_way == "menu_meas_func_phase") {
        var func = this.map.generator.riseFallTime(v, freq, offset, type, duration);
    }
    return func;
}
// определяем тип сигнала и в зависимости от типа задаем формулу, значение которой будем передавать на отрисовку 
CNT90.Generator.prototype.typeSignalFunc = function (type, value) {
	if (this.map.time == "undefined" || this.map.time == "Infinity") {
		this.map.time = 0;
	}
    var freq = parseInt(value.freq, 10);
    var v = value.v;
    var offset = value.offset;
    var duration = value.duration;
    if (this.map.menu_bar.current_pos_way != "menu_statplot_numerical") {
        this.current_pos_way = this.map.menu_bar.current_pos_way;
    }
    var deltaTime = 1 / (freq * 50);
    switch (type) {
        case "sin":
            if (this.current_pos_way == "menu_meas_func_freq_freq") {
                var func = freq;
                this.map.time += deltaTime;
                cnt90.state.graphic.timeBetweenMeasurements = deltaTime;
                break;
            }
            if (this.current_pos_way == "menu_meas_func_volt") {
                var func = v * Math.sin(2 * 3.14 * freq * this.map.time);
            } else {
                var func = this.map.generator.timeAndPhaseMenu(type, v, freq, offset, duration);				
            }
			this.map.time += deltaTime;
            cnt90.state.graphic.timeBetweenMeasurements = deltaTime;
            break;
        case "impulse":
            if (this.current_pos_way == "menu_meas_func_freq_freq") {
                var func = freq;
                this.map.time += deltaTime;
                cnt90.state.graphic.timeBetweenMeasurements = deltaTime;
                break;
            }
            if (this.current_pos_way == "menu_meas_func_volt") {
                var period = 1 / value.freq;
                var numOfCycles = parseInt(this.map.time / period);
                var timeOfLastPeriod = this.map.time - period * numOfCycles;
                if (timeOfLastPeriod < value.duration) {
                    var func = value.v;
                } else {
                    var func = 0;
                }
            } else {
                var func = this.map.generator.timeAndPhaseMenu(type, v, freq, offset, duration);
            }
			this.map.time += deltaTime;
            cnt90.state.graphic.timeBetweenMeasurements = deltaTime;
            
            break;
        case "frequencyModulation":
            var fc = freq;//1000;										//carrier signal frequency
            var fm = parseInt(value.modFreq, 10);//250				//modulating signal frequency
            var m = parseInt(value.modIndex, 10) * (1 / v);				//Modulation index
            var dw = m * fm;											//девиация частоты

            if (this.current_pos_way == "menu_meas_func_freq_freq") {
                //var func = freq;
                var sin = v * Math.sin(2 * 3.14 * fm * this.map.time);
                var func = fc + dw * sin;
                this.map.time += 1 / (fm * 50);
                cnt90.state.graphic.timeBetweenMeasurements = 1 / (fm * 50);
                break;
            }
            if (this.current_pos_way == "menu_meas_func_volt") {
                //var func_sin = Math.sin(2*180*freq*this.map.time+v*Math.cos(2*180*freq*this.map.time))
                //var func = 10*func_sin;		
                var func = v * Math.sin(2 * 3.14 * fc * this.map.time + (v * m * Math.sin(2 * 3.14 * fm * this.map.time)));

                this.map.time += 1 / (fm * 50);
                cnt90.state.graphic.timeBetweenMeasurements = 1 / (fm * 50);
            } else {
                var func = null;//this.map.generator.timeAndPhaseMenu(type, v, freq, offset, duration);
				this.map.time += deltaTime;
                cnt90.state.graphic.timeBetweenMeasurements = deltaTime;
            }
            break;
        case "amplitudeModulation":
            var fc = freq;											//carrier signal frequency
            var fm = parseInt(value.modFreq, 10);					//modulating signal frequency
            if (this.current_pos_way == "menu_meas_func_freq_freq") {
                var func = freq;
                this.map.time += 1 / (fm * 50);
                cnt90.state.graphic.timeBetweenMeasurements = 1 / (fm * 50);
                break;
            }
            if (this.current_pos_way == "menu_meas_func_volt") {
                //var func = v*Math.sin(2*180*freq*this.map.time);

                //var fc = freq;											//carrier signal frequency
                //var fm = parseInt(value.modFreq, 10);					//modulating signal frequency
                var m = parseFloat(value.modIndex) * (1 / v);				//Modulation index

                var c = v * Math.sin(2 * 3.14 * fc * this.map.time);
                var M = v * Math.sin(2 * 3.14 * fm * this.map.time);

                var func = (1 + m * M) * c;

                this.map.time += 1 / (fm * 50);
                cnt90.state.graphic.timeBetweenMeasurements = 1 / (fm * 50);
            } else {
                var func = null;//this.map.generator.timeAndPhaseMenu(type, v, freq, offset, duration);
				this.map.time += deltaTime;
                cnt90.state.graphic.timeBetweenMeasurements = deltaTime;
            }
            break;
        case "NaN":
            var func = 0;
            break;
    };
    return func;
}

// Смотрим в каких пунктах меню, мы можем считывать параметры сигнала
CNT90.Generator.prototype.selectedInput = function () {
    var selected_item = this.map.menu_bar.menu_elements_configuration[this.map.menu_bar.current_pos_way][this.map.menu_bar.current_index].key;
    if (this.map.menu_bar.current_pos_way == "menu_freq_ratio" ||
        this.map.menu_bar.current_pos_way == "menu_meas_func_freq_freq" ||
        this.map.menu_bar.current_pos_way == "menu_meas_func_volt" ||
        //this.map.menu_bar.current_pos_way == "menu_statplot_numerical" ||
        this.map.menu_bar.current_pos_way == "time_interval" ||
        this.map.menu_bar.current_pos_way == "rise_fall_time" ||
        this.map.menu_bar.current_pos_way == "pulse_width" ||
        this.map.menu_bar.current_pos_way == "duty" ||
        this.map.menu_bar.current_pos_way == "menu_meas_func_phase") {
        cnt90.state.graphic.curInput = selected_item;
        cnt90.setStatPlotInDefault();
        this.map.generator.initParamSignal();
        this.map.generator.hideGraphPlot();
    }
}

// Вызывается по кнопке Hold/Run, смотрим в каких случаях будет доступна функция запуска/остановки
CNT90.Generator.prototype.actionInput = function () {
    var selected_item = this.map.menu_bar.menu_elements_configuration[this.map.menu_bar.current_pos_way][this.map.menu_bar.current_index].key;
    if (this.map.menu_bar.current_pos_way == "menu_freq_ratio"
        || this.map.menu_bar.current_pos_way == "menu_meas_func_freq_freq"
        || this.map.menu_bar.current_pos_way == "menu_meas_func_volt"
        || this.map.menu_bar.current_pos_way == "menu_statplot_numerical"
        || this.map.menu_bar.current_pos_way == "time_interval"
        || this.map.menu_bar.current_pos_way == "rise_fall_time"
        || this.map.menu_bar.current_pos_way == "pulse_width"
        || this.map.menu_bar.current_pos_way == "duty"
        || this.map.menu_bar.current_pos_way == "menu_meas_func_phase") {
        if (cnt90.timer == null) {
            this.map.generator.startSignal();
            cnt90.generator.signal();
            this.map.state.startGenerator = '1';



        } else {
            this.map.generator.stopSignal();
            cnt90.generator.signal();
            this.map.state.startGenerator = '0';
        }
    }
}

// передаем сюда значение функции и выводим на экран ( Вызывается каждую секунду)
/*CNT90.Generator.prototype.signal = function () {
  //cnt90.state.text = cnt90.generator.functionSignal();
  cnt90.generator.limits();
  cnt90.draw();
  cnt90.action(this, 'signal', cnt90.state.text);
  cnt90.SetStatPlotParam(cnt90.state.text);
}
*/

CNT90.Generator.prototype.limits = function () {
    var value = cnt90.generator.functionSignal();
    if (value == undefined) {
        return;
    }
    switch (this.map.state.limits.state) {
        case 'OFF':
            cnt90.state.text = value;
            cnt90.state.presintationModeOfValue = this.map.transferSystem(value);
            break;
        case 'CAPTURE':
            this.map.generator.limitsMode(value);
            if (this.map.state.limits.mode == 'RANGE' && value < this.map.state.limits.upperLimit || value > this.map.state.limits.lowerLimit) {
                cnt90.state.text = value;
                cnt90.state.presintationModeOfValue = this.map.transferSystem(value);
                this.map.control('graph').setYAxisAndRedraw(this.map.state.limits.lowerLimit, this.map.state.limits.upperLimit, 0);
            }
            if (this.map.state.limits.mode == 'BELOW' && value < this.map.state.limits.upperLimit) {
                cnt90.state.text = value;
                cnt90.state.presintationModeOfValue = this.map.transferSystem(value);
                this.map.control('graph').setYAxisAndRedraw(this.map.state.limits.lowerLimit, this.map.state.stat_plot.max, 0);
            }
            if (this.map.state.limits.mode == 'ABOVE' && value > this.map.state.limits.lowerLimit) {
                cnt90.state.text = value;
                cnt90.state.presintationModeOfValue = this.map.transferSystem(value);
                this.map.control('graph').setYAxisAndRedraw(this.map.state.stat_plot.min, this.map.state.limits.upperLimit, 0);
            }
            break;
        case 'ALARM':
            this.map.generator.limitsMode(value);
            cnt90.state.text = value;
            cnt90.state.presintationModeOfValue = this.map.transferSystem(value);
            break;
        case 'ALARM_STOP':
            this.map.generator.limitsMode(value);
            cnt90.state.text = value;
            cnt90.state.presintationModeOfValue = this.map.transferSystem(value);
            if (this.map.state.limits.indLimShow) {
                this.map.generator.stopSignal();
            }
            break;
    };
}


CNT90.Generator.prototype.limitsMode = function (value) {
    switch (this.map.state.limits.mode) {
        case 'RANGE':
            this.map.state.limits.indLimShow = (value > this.map.state.limits.upperLimit ||
                                            value < this.map.state.limits.lowerLimit);
            break;
        case 'BELOW':
            this.map.state.limits.indLimShow = (value > this.map.state.limits.upperLimit);
            break;
        case 'ABOVE':
            this.map.state.limits.indLimShow = (value < this.map.state.limits.lowerLimit);
            break;
    };
}
// в зависимости от выбранного входа и элемента меню передаем в генератор значение функции
CNT90.Generator.prototype.functionSignal = function () {
    var pi = Math.PI;
    var valueSignal = this.map.generator.getParamSignal();
    console.log(valueSignal[0]);
    var selected_item = this.map.state.graphic.curInput;//this.map.menu_bar.menu_elements_configuration[this.map.menu_bar.current_pos_way][this.map.menu_bar.current_index].key;
    switch (selected_item) {
        case "meas_func_freq_input_a":
            if (this.map.state.signalA.typeSignal == "NaN") break;
            if (this.map.state.inputA.input) {
                var func = this.map.generator.typeSignalFunc(this.map.state.signalA.typeSignal, valueSignal[0]);
                this.map.state.signalA.freq = func;
            }
            break;
        case "meas_func_freq_input_b":
            if (this.map.state.signalB.typeSignal == "NaN") break;
            if (this.map.state.inputB.input) {
                var func = this.map.generator.typeSignalFunc(this.map.state.signalB.typeSignal, valueSignal[1]);
                this.map.state.signalB.freq = func;
            }
            break;
        case "meas_func_freq_input_c":
            if (this.map.state.signalC.typeSignal == "NaN") break;
            if (this.map.state.inputC.input) {
                var func = this.map.generator.typeSignalFunc(this.map.state.signalC.typeSignal, valueSignal[2]);
                this.map.state.signalC.freq = func;
            }
            break;
        case "meas_func_volt_input_a":
            if (this.map.state.signalA.typeSignal == "NaN") break;
            if (this.map.state.inputA.input) {
                var func = this.map.generator.typeSignalFunc(this.map.state.signalA.typeSignal, valueSignal[0]);
                this.map.state.signalA.v = func;
            }
            break;
        case "meas_func_volt_input_b":
            if (this.map.state.signalB.typeSignal == "NaN") break;
            if (this.map.state.inputB.input) {
                var func = this.map.generator.typeSignalFunc(this.map.state.signalB.typeSignal, valueSignal[1]);
                this.map.state.signalB.v = func;
            }
            break;
        case "meas_func_volt_input_c":
            if (this.map.state.signalC.typeSignal == "NaN") break;
            if (this.map.state.inputC.input) {
                var func = this.map.generator.typeSignalFunc(this.map.state.signalC.typeSignal, valueSignal[2]);
                this.map.state.signalC.v = func;
            }
            break;
        case "A/B":
            if (this.map.state.signalA.typeSignal == "NaN" || this.map.state.signalB.typeSignal == "NaN") break;
            if (this.map.state.inputA.input && this.map.state.inputB.input) {
                var func = valueSignal[0].freq / valueSignal[1].freq;
                this.map.state.signalA.freq = valueSignal[0].freq;
                this.map.state.signalB.freq = valueSignal[1].freq;
            }
            break;
        case "B/A":
            if (this.map.state.signalA.typeSignal == "NaN" || this.map.state.signalB.typeSignal == "NaN") break;
            if (this.map.state.inputA.input && this.map.state.inputB.input) {
                var func = valueSignal[1].freq / valueSignal[0].freq;
                this.map.state.signalA.freq = valueSignal[0].freq;
                this.map.state.signalB.freq = valueSignal[1].freq;
            }
            break;
        case "C/A":
            if (this.map.state.signalA.typeSignal == "NaN" || this.map.state.signalC.typeSignal == "NaN") break;
            if (this.map.state.inputA.input && this.map.state.inputC.input) {
                var func = valueSignal[2].freq / valueSignal[0].freq;
                this.map.state.signalA.freq = valueSignal[0].freq;
                this.map.state.signalC.freq = valueSignal[2].freq;
            }
            break;
        case "С/B":
            if (this.map.state.signalB.typeSignal == "NaN" || this.map.state.signalC.typeSignal == "NaN") break;
            if (this.map.state.inputB.input && this.map.state.inputС.input) {
                var func = valueSignal[2].freq / valueSignal[1].freq;
                this.map.state.signalС.freq = valueSignal[2].freq;
                this.map.state.signalB.freq = valueSignal[1].freq;
            }
            break;
        case "IntervalAtoB":
            if (this.map.state.signalA.typeSignal == "NaN" || this.map.state.signalB.typeSignal == "NaN") break;
            if (this.map.state.inputA.input && this.map.state.inputB.input) {
                var f1 = this.map.generator.typeSignalFunc(this.map.state.signalA.typeSignal, valueSignal[0]);
                var f2 = this.map.generator.typeSignalFunc(this.map.state.signalB.typeSignal, valueSignal[1]);
                var func = Math.abs(f2[1] - f1[0]);
                this.map.state.timeInterval.AtoB = func;
            }
            break;
        case "IntervalBtoA":
            if (this.map.state.signalA.typeSignal == "NaN" || this.map.state.signalB.typeSignal == "NaN") break;
            if (this.map.state.inputA.input && this.map.state.inputB.input) {
                var f1 = this.map.generator.typeSignalFunc(this.map.state.signalA.typeSignal, valueSignal[0]);
                var f2 = this.map.generator.typeSignalFunc(this.map.state.signalB.typeSignal, valueSignal[1]);
                var func = Math.abs(f1[1] - f2[0]);
                this.map.state.timeInterval.BtoA = func;
            }
            break;
        case "rise_input_a":
            if (this.map.state.signalA.typeSignal == "NaN") break;
            if (this.map.state.inputA.input) {
                var f = this.map.generator.typeSignalFunc(this.map.state.signalA.typeSignal, valueSignal[0]);
                var func = f[0]; // rise
                this.map.state.signalA.rise = func;
            }
            break;
        case "fall_input_a":
            if (this.map.state.signalA.typeSignal == "NaN") break;
            if (this.map.state.inputA.input) {
                var f = this.map.generator.typeSignalFunc(this.map.state.signalA.typeSignal, valueSignal[0]);
                var func = f[1]; // fall
                this.map.state.signalA.rise = func;
            }
            break;
        case "rise_input_b":
            if (this.map.state.signalB.typeSignal == "NaN") break;
            if (this.map.state.inputB.input) {
                var f = this.map.generator.typeSignalFunc(this.map.state.signalB.typeSignal, valueSignal[1]);
                var func = f[0]; // rise
                this.map.state.signalA.rise = func;
            }
            break;
        case "fall_input_b":
            if (this.map.state.signalB.typeSignal == "NaN") break;
            if (this.map.state.inputB.input) {
                var f = this.map.generator.typeSignalFunc(this.map.state.signalB.typeSignal, valueSignal[1]);
                var func = f[1]; // fall
                this.map.state.signalB.rise = func;
            }
            break;
        case "pulse_width_a":
            if (this.map.state.signalA.typeSignal == "NaN") break;
            if (this.map.state.inputA.input) {
                var func = this.map.generator.typeSignalFunc(this.map.state.signalA.typeSignal, valueSignal[0]);
                this.map.state.signalA.pulseW = func;
            }
            break;
        case "pulse_width_b":
            if (this.map.state.signalB.typeSignal == "NaN") break;
            if (this.map.state.inputB.input) {
                var func = this.map.generator.typeSignalFunc(this.map.state.signalB.typeSignal, valueSignal[1]);
                this.map.state.signalB.pulseW = func;
            }
            break;
        case "duty_a":
            if (this.map.state.signalA.typeSignal == "NaN") break;
            if (this.map.state.inputA.input) {
                var func = this.map.generator.typeSignalFunc(this.map.state.signalA.typeSignal, valueSignal[0]);
                this.map.state.signalA.duty = func;
            }
            break;
        case "duty_b":
            if (this.map.state.signalB.typeSignal == "NaN") break;
            if (this.map.state.inputB.input) {
                var func = this.map.generator.typeSignalFunc(this.map.state.signalB.typeSignal, valueSignal[1]);
                this.map.state.signalB.duty = func;
            }
            break;
        case "A_and_B":
            if (this.map.state.signalA.typeSignal == "NaN" || this.map.state.signalB.typeSignal == "NaN") break;
            if (this.map.state.inputA.input && this.map.state.inputB.input) {
                var f1 = this.map.generator.typeSignalFunc(this.map.state.signalA.typeSignal, valueSignal[0]);
                var f2 = this.map.generator.typeSignalFunc(this.map.state.signalB.typeSignal, valueSignal[1]);
                var func = (f1[0] - f2[0]) * 360 * valueSignal[0].freq;
                this.map.state.phase.AandB = func;
            }
            break;
        case "A_and_C":
            if (this.map.state.signalA.typeSignal == "NaN" || this.map.state.signalC.typeSignal == "NaN") break;
            if (this.map.state.inputA.input && this.map.state.inputC.input) {
                var f1 = this.map.generator.typeSignalFunc(this.map.state.signalA.typeSignal, valueSignal[0]);
                var f2 = this.map.generator.typeSignalFunc(this.map.state.signalC.typeSignal, valueSignal[2]);
                if (f1[0] - f2[0] > 0) {
                    var func = (f1[0] - f2[0]) * 360 * valueSignal[0].freq;
                } else var func = 0;
                this.map.state.phase.AandC = func;
            }
            break;
        case "B_and_C":
            if (this.map.state.signalB.typeSignal == "NaN" || this.map.state.signalC.typeSignal == "NaN") break;
            if (this.map.state.inputB.input && this.map.state.inputC.input) {
                var f1 = this.map.generator.typeSignalFunc(this.map.state.signalB.typeSignal, valueSignal[1]);
                var f2 = this.map.generator.typeSignalFunc(this.map.state.signalC.typeSignal, valueSignal[2]);
                if (f1[0] - f2[0] > 0) {
                    var func = (f1[0] - f2[0]) * 360 * valueSignal[1].freq;
                } else var func = 0;
                this.map.state.phase.BandC = func;
            }
            break;
    };
    return func;
}

// передаем сюда значение функции и выводим на экран ( Вызывается каждую секунду)
CNT90.Generator.prototype.signal = function () {
    // для чего?
    if (cnt90.state.text == 0) {
        cnt90.generator.actionSignal();
    }
    //
    if (cnt90.state != undefined && cnt90.state.text != undefined) {
        /*var tmpval = cnt90.generator.functionSignal();
        if (tmpval != undefined)*/
        cnt90.generator.limits();
        cnt90.draw();
        cnt90.action(this, 'signal', cnt90.state.text);
        if (typeof cnt90.state.text === "undefined") {
            alert("WARN");
        }
        cnt90.SetStatPlotParam(cnt90.state.text);
        cnt90.generator.setStatistic();
    }

}



CNT90.Generator.prototype.getState = function () {
    //this.map.generator.startSignal();
    // cnt90.SetStatPlotParam(cnt90.state.text);
    //для чего IsOn ? (и где он у нас в state?)
    // if (this.map.state.IsOn)
    cnt90.generator.setStatistic();
}

CNT90.Generator.prototype.setStatistic = function () {
    //для чего IsOn ? (и где он у нас в state?)
    //this.map.state.IsOn = true;
    var stat_plot_config = {
        "stat_plot_mean": this.map.state.stat_plot.N,
        "stat_plot_max": this.map.state.stat_plot.MaxPM,
        "stat_plot_min": this.map.state.stat_plot.MinPM,
        "stat_plot_p_p": this.map.state.stat_plot.P_PPM,
        "stat_plot_adev": this.map.state.stat_plot.AdevPM,
        "stat_plot_std": this.map.state.stat_plot.StdPM
    }

    for (var i = 0; i < this.map.menu_bar.menu_elements.length; i++) {
        for (var key in stat_plot_config) {
            if (key == this.map.menu_bar.menu_elements_configuration[this.map.menu_bar.current_pos_way][i].key) {
                this.map.menu_bar.menu_elements[i].setText(stat_plot_config[key]);
            }
        }
    }
}
CNT90.Generator.prototype.showGraphic = function () {
    if (!this.map.state.graphic.isDisplay) {
        $('#placeholder').css("display", "block");
    } else {
        this.map.setMenu("menu_statplot_numerical");
        this.map.resetMenu(true);
        $('#placeholder').css("display", "none");
    }
}

CNT90.Generator.prototype.hideGraphPlot = function () {
    this.map.control('graph').clearPlot();
    $('#placeholder').css("display", "none");
}

CNT90.Generator.prototype.addSignal = function (index, signal) {
    var valueSignal = this.getParamSignal();
    signal.typeSignal = valueSignal[index].typeSignal;
    signal.v = valueSignal[index].v;
    signal.freq = valueSignal[index].freq;
    signal.duration = valueSignal[index].duration;
	//signal.t = valueSignal[index].t;
    signal.offset = valueSignal[index].offset;
    if (signal.typeSignal == "frequencyModulation" || signal.typeSignal == "amplitudeModulation") {
        signal.modFreq = valueSignal[index].modFreq;
        signal.modIndex = valueSignal[index].modIndex;
    } else {
        signal.modFreq = 0;
        signal.modIndex = 0;
    }
}

// при нажатии на Value записываем считанную частоту в конфиг, при условии что входы подключены
CNT90.Generator.prototype.initParamSignal = function () {
    //var valueSignal = this.getParamSignal();
    //var selected_item = this.map.menu_bar.menu_elements_configuration[this.map.menu_bar.current_pos_way][this.map.menu_bar.current_index].key;
    if (this.map.state.inputA.input /*&& selected_item == "meas_func_freq_input_a"*/) {
        this.map.generator.addSignal(0, this.map.state.signalA);
    }
    if (this.map.state.inputB.input/* && selected_item == "meas_func_freq_input_b"*/) {
        this.map.generator.addSignal(1, this.map.state.signalB);
    }
    if (this.map.state.inputC.input/* && selected_item == "meas_func_freq_input_c"*/) {
        this.map.generator.addSignal(2, this.map.state.signalC);
    }
    if (this.map.state.inputA.input && this.map.state.inputB.input/* && selected_item == "A/B" || selected_item == "B/A"*/) {
        this.map.generator.addSignal(0, this.map.state.signalA);
        this.map.generator.addSignal(1, this.map.state.signalB);
    }
    if (this.map.state.inputA.input && this.map.state.inputC.input/* && selected_item == "C/A"*/) {
        this.map.generator.addSignal(0, this.map.state.signalA);
        this.map.generator.addSignal(2, this.map.state.signalC);
    }
    if (this.map.state.inputB.input && this.map.state.inputC.input/* && selected_item == "C/B"*/) {
        this.map.generator.addSignal(1, this.map.state.signalB);
        this.map.generator.addSignal(2, this.map.state.signalC);
    }
}

// запускаем генерацию сигнала
CNT90.Generator.prototype.startSignal = function () {
    var time = 1000;
    cnt90.timer = setInterval(this.signal, time);
}

// останавливаем генерацию сигнала
CNT90.Generator.prototype.stopSignal = function () {
    if (cnt90.timer != null)
        clearInterval(cnt90.timer);
    cnt90.timer = null;
}

// парсим сигнал, который вбиваем руками в html-инпуты
CNT90.Generator.prototype.parseCustomSignal = function(param) {
  var gen = $('#attrGen');
	var valueSignal = {};
	valueSignal.typeSignal = gen.find('[name=type'+ param +']:checked').val();
	valueSignal.v = gen.find('[name=amp'+ param +']').val() / 1000;
	valueSignal.duration = gen.find('[name=t' + param + ']').val() / 1000000000;
	
	valueSignal.offset = gen.find('[name=of'+ param +']').val() / 1000000000;
	if (gen.find('[name=signal'+ param +']:checked').val() == 'hz')
		valueSignal.freq = gen.find('[name=hz'+ param +']').val() / 1;
	else
		valueSignal.freq = 1000000000 / gen.find('[name=period'+ param +']').val();
	var p = 1 / valueSignal.freq;
	
	if (param == "A" || param == "B") {
		if (valueSignal.freq > 300000000) {
			alert('Некоректные данные на входе ' + param + ": частота превышает 300 МГц.");
      this.stopSignal();
			return null;
		}
	}
	
	if (param == "C") {
		if (valueSignal.freq > 8000000000) {
			alert('Некоректные данные на входе ' + param + ": частота превышает 8 ГГц.");
      this.stopSignal();
			return null;
		}
	}
	if (p <= valueSignal.duration) {
    alert('Некоректные данные на входе ' + param + ': период меньше или равен длительности');
    this.stopSignal();
    return null;
  }
  
	if (valueSignal.freq <= 0 || valueSignal.v <= 0 || valueSignal.duration <= 0) {
		alert('Некоректные данные на входе ' + param + ": один из параметров задан неположительным числом.");
    this.stopSignal();
		return null;
	}
	console.log(valueSignal);
	return valueSignal;
}

// парсим готовый набор сигналов
CNT90.Generator.prototype.parseStaticSignal = function (input) {
    var valueSignal = {};
    valueSignal.typeSignal = input[0];//val.split('|')[0];
    valueSignal.v = input[1] / 1000;
    valueSignal.freq = input[2] / 1;    
    valueSignal.duration = ((1/valueSignal.freq)/2);
	//valueSignal.t = input[3] / 1000000000;
    
    valueSignal.offset = input[4] / 1000000000;
    if (valueSignal.typeSignal == "frequencyModulation" || valueSignal.typeSignal == "amplitudeModulation") {
        valueSignal.modFreq = input[5] / 1;
        valueSignal.modIndex = input[6] / 1;
    } else {
        valueSignal.modFreq = 0;
        valueSignal.modIndex = 0;
    }
    console.log(valueSignal);
    return valueSignal;
}

// получаем из таблицы значения сигналов
CNT90.Generator.prototype.getParamSignal = function () {
    var gen = $('#attrGen');
    var valA = gen.find('[name=inputA]:checked').val();
    var valB = gen.find('[name=inputB]:checked').val();
    var valC = gen.find('[name=inputC]:checked').val();
    var inputA = valA.split('|');
    var inputB = valB.split('|');
    var inputC = valC.split('|');
    var valueSignalA = (valA == 0) ? this.parseCustomSignal('A') : this.parseStaticSignal(inputA);
    var valueSignalB = (valB == 0) ? this.parseCustomSignal('B') : this.parseStaticSignal(inputB);
    var valueSignalC = (valC == 0) ? this.parseCustomSignal('C') : this.parseStaticSignal(inputC);
    return [valueSignalA, valueSignalB, valueSignalC];
}


/*

CNT90.Generator.prototype.setParamSignal = function (sin, v, freq, t, offset) {
	$("#attrGen input[type='radio'][value='0']").attr("checked", "checked");
	if (sin != true)
		$('#attrGen input[name="type"]').attr("checked", "checked");
	else
		$('#attrGen input[name="type"]').attr("checked", "unchecked");
	$('#attrGen input[name="amp"]').attr("value", v);
	$('#attrGen input[name="t"]').attr("value", t);
	$('#attrGen input[name="of"]').attr("value", offset);
	$('#attrGen input[name="hz"]').attr("value", freq);
	$('#attrGen input[value="hz"]').attr("checked", "checked");
}
*/

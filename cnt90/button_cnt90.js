function ButtonCNT90(name, param) {
    ButtonCNT90.superclass.constructor.call(this, name, param);
    this.stateChange = param;
}
extend(ButtonCNT90, Button);

ButtonCNT90.prototype.setMap = function (map) {
    ButtonCNT90.superclass.setMap.call(this, map);
}

ButtonCNT90.prototype.stateChangePerform = function (state, new_state, action) {
    var b = this;
    var map = this.map;
    var s = state;
    var d = new_state;
    //сделано для упражнений. 
    d.button_push = this.name;

    if (typeof (action) === 'string') {
        eval(action);
    }
    else if (action instanceof Array) {
        for (var i = 0; i < action.length; i++) {
            this.stateChangePerform(s, d, action[i]);
        }
    }
    else {
        var cond = 1;
        if (action.cond != undefined) {
            cond = eval(action.cond);
        }
        if (cond) eval(action.act);
    }
}


ButtonCNT90.prototype.action = function () {

    this.map.action(this, 'button_click');
    var d = this.map.state
    this.stateChangePerform(d, d, this.stateChange);

    this.map.action(this, 'button_post');

    this.map.action(this, 'state_change');


}

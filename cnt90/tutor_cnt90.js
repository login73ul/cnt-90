TutorCNT90 = function (name, param) {
	TutorCNT90.superclass.constructor.call(this, name, param);
}

extend(TutorCNT90, Tutor);

TutorCNT90.prototype.revertMapState = function() {
  this.map.restoreFromMemento(this.safe_memento);
  this.map.resetMenu();
  if(!this.map.state.socket) {
    this.map.state = new State();
  }
  this.map.draw();
}

TutorCNT90.prototype.isCorrectAnswer = function () {
  return this.new_state == this.ex.nodes[this.next_goal_state].hash;
}

TutorCNT90.prototype.loadExercise = function(ex) {
	this.ex = ex;
	this.next_goal_state = 0;
	this.fail_count = 0;
	this.prev_state = this.map.state;
	this.map.restart();

  this.map.GetControlByName("tutor").safe_memento = new Memento(this.map.state);
	
	//this.hash_pool = [];
	this.new_state = this.map.state;

	if (ex.code != undefined) {
		eval(ex.code);
		alert(ex.code);
	}

	this.makeMenuItem();
	this.showCurrentGoal();
}



TutorCNT90.prototype.checkStateChanges = function () {
  var but_push = this.map.state.button_push;
	var prev_goal_num = this.next_goal_state;
	var _prev_state = this.map.state;
	if (this.ex === undefined || this.next_goal_state == -1)
	    return;
	if (this.isCorrectAnswer()) {
	    window.genAold = cnt90.actionManager.map.state.genA;
	    window.genBold = cnt90.actionManager.map.state.genB;
	    window.genCold = cnt90.actionManager.map.state.genC;
	    this.setNextGoal();
	} else {
	    switch (this.map.mode) {
	        case deviceMode.TRAINING:
	            if ((but_push == "up") || (but_push == "down") || (but_push == "right") || (but_push == "left")) {
	                //this.map.restoreFromMemento(this.safe_memento);
	                return;
	            }

	            $('input:radio[name=inputA][value="' + window.genAold + '"]').prop('checked', true);
	            cnt90.actionManager.map.state.genA = window.genAold;
	            $('input:radio[name=inputB][value="' + window.genBold + '"]').prop('checked', true);
	            cnt90.actionManager.map.state.genB = window.genBold;
	            $('input:radio[name=inputC][value="' + window.genCold + '"]').prop('checked', true);
	            cnt90.actionManager.map.state.genC = window.genCold;


	            if (but_push != "radio") {
	                this.revertMapState();
	            }

	            this.next_goal_state = prev_goal_num;
	            this.new_state = this.map.state;
	            alert("Ошибка!");
	            return;
	        case deviceMode.CONTROL:
	            if ((but_push == "up") || (but_push == "down") || (but_push == "right") || (but_push == "left")) {
	                //this.map.restoreFromMemento(this.safe_memento);
	                return;
	            }
	            $('input:radio[name=inputA][value="' + window.genAold + '"]').prop('checked', true);
	            cnt90.actionManager.map.state.genA = window.genAold;
	            $('input:radio[name=inputB][value="' + window.genBold + '"]').prop('checked', true);
	            cnt90.actionManager.map.state.genB = window.genBold;
	            $('input:radio[name=inputC][value="' + window.genCold + '"]').prop('checked', true);
	            cnt90.actionManager.map.state.genC = window.genCold;


	            if (but_push != "radio") {
	                this.revertMapState();
	            }

	            this.next_goal_state = prev_goal_num;
	            this.new_state = this.map.state;
	            alert("Ошибка!");
	            this.fail_count++;
	            if (this.fail_count >= 5)
	                this.Abort();
	            return;
	    }
	}
}

var cnt90;

function setMode(map, mode) {
    map.setMode(mode);
    map.state = new State();
    if (map.menu_bar) {
        cnt90.resetMenu();
    }
    map.draw();
    switch (mode) {
        case deviceMode.EMULATOR:
            map.start();
            if (map.state.startGenerator) {
                map.generator.startSignal();
            }
            $("h3#mode_header").text("Режим эмулятора");
            /*if (!map.state.graphic.isDisplay) {
              $('#placeholder').css("display", "block");
            }*/
            break;
        case deviceMode.HELP:
            $("text").css("display", "none");
            if (map.state.startGenerator) {
                map.generator.stopSignal();
            }
            if (!map.state.graphic.isDisplay) {
                $('div#placeholder').css("display", "none");
            }

            $("h3#mode_header").text("Режим справки");

            map.stop();
            break;
        case deviceMode.TRAINING:
            // если только что зашли в режим тренировки - сбрасываем стейт
            this.map.state = new State();



        case deviceMode.CONTROL:
            if (map.menu_bar) {
                map.menu_bar.menuRectangeRedraw();
            }

            if (map.state.startGenerator) {
                map.generator.stopSignal();
            }
            if (!map.state.graphic.isDisplay) {
                $('div#placeholder').css("display", "none");
            }

            $("h3#mode_header").text("Режим тренажера");
            var bar = map.GetControlByName('tutor');

            bar.unloadExercise();
            var foo = map.GetExList();
            var qux = map;
            map.RemoveAdditionalMenuItems();
            var html_s = "";
            for (var i = 0; i < foo.length; i++) {
                html_s += "<li><a href=\"#\" id=\"ex" + i + "\">" + foo[i] + "</a></li>"
            }
            map.AddAdditionalMenuItem("exercise_list", "Задания", "<ul>" + html_s + "</ul>");
            $('div#menu div:last a').each(function (index) { $(this).click(function () { bar.loadExercise(qux.GetEx(index)) }) });
            map.FocusOnAdditionalMenuItem(1);
            break;
            //case deviceMode.CONTROL:
            //map.restart();
            //break;
    }

}



$(document).ready(function () {
    $('#tabs').tabs({ heightStyle: "fill" });

    var cnt90_menu = {};
    cnt90 = new DeviceCNT90($('#map'), 1200, 567);
    cnt90.renderAreaControl();
    //cnt90_menu.initToltips();
    cnt90.setMenu("menu_input");
    cnt90.resetMenu();
    cnt90.draw();


    $('input[type=radio][name=inputA]').click(function () {
        window.genAold = cnt90.actionManager.map.state.genA;
        cnt90.state.genA = this.value;
        cnt90.state.button_push = "radio";
        var control = new Control();
        cnt90.actionManager.click(control);
    });
    $('input[type=radio][name=inputB]').click(function () {
        window.genBold = cnt90.actionManager.map.state.genB;
        cnt90.state.genB = this.value;
        cnt90.state.button_push = "radio";
        var control = new Control();
        cnt90.actionManager.click(control);
    });
    $('input[type=radio][name=inputC]').click(function () {
        window.genCold = cnt90.actionManager.map.state.genC;
        cnt90.state.genC = this.value;
        cnt90.state.button_push = "radio";
        var control = new Control();
        cnt90.actionManager.click(control);
    });
});


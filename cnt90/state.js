//TODO: Refactor constructor and create new methods for setters and initialization
function State() {
    this.current_menu_way = "menu_input";
    this.measurement = "";
    this.activeInput = null;
    this.measuredCharacteristic = "";
    this.currentCharacteristic = "Frequency";
    this.socket = 0;
    this.power = 0;
    this.trigA = 0;
    this.gate = 0;
    this.trigB = 0;
    this.text = '0';
    this.button_push = '1';
    this.hist = '1';
    this.startGenerator = '0';
    this.timeInterval = { AtoB: 0, BtoA: 0}
    this.phase = {AandB: 0, AandC: 0, BandC: 0}
    this.meas_ment = "Hz";
    this.presintationModeOfValue = "0";
    this.signalA = {
        freq: 0, t: 0, v: 0, duration: 0, sin: 0, offset: 0, typeSignal: "NaN", rise: 0, fall: 0, pulseW: 0, duty: 0, modFreq: 0, modIndex: 0
    };
    this.signalB = {
        freq: 0, t: 0, v: 0, duration: 0, sin: 0, offset: 0, typeSignal: "NaN", rise: 0, fall: 0, pulseW: 0, duty: 0, modFreq: 0, modIndex: 0
    };
    this.signalC = {
        freq: 0, t: 0, v: 0, duration: 0, sin: 0, offset: 0, typeSignal: "NaN", modFreq: 0, modIndex: 0
    };
    this.inputA = {
        input: 0, front: 'drop_minus', coupling: 'ac', impedance: 'R1', attenuator: 'fading1', triggerLevel: 'start_auto', triggerSlope: 0.0, filter: 'an_lp'
    };
    this.inputB = {
        input: 0, front: 'drop_minus', coupling: 'ac', impedance: 'R1', attenuator: 'fading1', triggerLevel: 'start_auto', triggerSlope: 0.0, filter: 'an_lp'
    };
    this.inputC = {
        input: 0, front: 'drop_minus', coupling: 'ac', impedance: 'R1', attenuator: 'fading1', triggerLevel: 'start_auto', triggerSlope: 0.0, filter: 'an_lp'
    };
    this.arming = {
        start: 'OFF', startSlope: 'POS', startArmDelay: 0, stop: 'OFF', stopSlope: 'POS'
    };
    this.holdOff = {
        state: 'OFF', time: 200 //sec
    };
    this.timeOut = {
        state: 'OFF', time: 0.1 //sec
    };
    this.statistics = {
        statistic: "OFF", numOfSamples: 100, numOfBins: 20, pacingState: 'OFF', pacingTime: 0.02 //sec
    };
    this.mathematics = {
        math: 'OFF', mathContants: { K: 1, L: 0, N: 1 }
    };
    this.limits = {
        indLimShow: 0, state: 'OFF', mode: 'RANGE', lowerLimit: -0.01, upperLimit: 0.01, lowerLimitCount: -2, lowerLimitMantissa: -1, upperLimitCount: -2, upperLimitMantissa: 1
    };
    this.burst = {
        syncDelay: 400 /*sec*/, startDelay: 0, measTime: 200 /*sec*/, freqLimit: 400 /*MHz*/
    };
    this.miscellaneous = {
        func: 'FREQ A', smartFrequency: 'AUTO', smartTimeInterval: 'OFF', measTime: 0.2 /*sec*/, autoTrigLowFreq: 100 /*Hz*/,
        timebaseReference: 'AUTO', blankDigits: 0
    },
    this.graphic = {
        isDisplay: 0,
        curInput: 'meas_func_freq_input_a',
		timeBetweenMeasurements: 1
    }
    this.stat_plot = {
        Max: undefined, MaxPM: undefined, Min: undefined, MinPM: undefined, P_P: 0, P_PPM: 0, Adev: 0, AdevPM: 0, Std: 0, StdPM: 0, N: 1, SumFreq: 0, PrevFreq: 0, M: 1
    }
    this.genA = "NaN|0|0|0|0";
    this.genB = "NaN|0|0|0|0";
    this.genC = "NaN|0|0|0|0";
}


function Memento(state_to_save) {
  this.state = jQuery.extend(true, {}, state_to_save);

}

Memento.prototype.getSavedState = function() {
  return this.state;
}

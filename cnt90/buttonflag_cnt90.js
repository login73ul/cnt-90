function ButtonFlagCNT90(name, param) {
  ButtonBoxCNT90.superclass.constructor.call(this, name, param);
  this.param = param;
}
extend(ButtonFlagCNT90, ButtonCNT90);

ButtonFlagCNT90.prototype.setRender = function(render) {
	ButtonFlagCNT90.superclass.setRender.call(this, render);
}

ButtonFlagCNT90.prototype.setMap = function (map) {
	ButtonBox.superclass.setMap.call(this, map);
		
	map.addListener('state_change', this, function() {
		var t = map.state;
		m = eval(this.param.on);
		this.setLight(m);
	})
}

ButtonFlagCNT90.prototype.setLight = function (on) {
	this.light = on;
	if(on)
		this.render.lightOnOpacity(0.5);
	else
		this.render.lightOff();
}

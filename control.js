//Базовый класс для контроллеров, наследуется от logic
//Своёство
//render - компонент который отвечает за отображение в режиме эмулятора
//renderControl - компонент который отвечает за отображение в режиме справки

//Создание (имя, параметры)
function Control(name, param){
  Control.superclass.constructor.call(this, name, param);
}
extend(Control, Logic);

//Имеет ли внешнее отображение - да
Control.prototype.haveArea = function () {
  return true;
}

//Установка рендера, компонент который отвечает за отображение
Control.prototype.setRender = function(render) {
  this.render = render;
  this.renderControl = jQuery.extend(true, {}, render);
}

//Отрисовка контроллера в режиме эмулятора
Control.prototype.draw = function () {
  this.renderControl.hide();
  this.render.render(this.map);
  this.render.show();
}

//Отрисовка контроллера в режиме справки
Control.prototype.drawControl = function () {
  this.render.hide();
  this.renderControl.renderControl(this.map);
  this.renderControl.show();
}

//Возвращет габариты контролера (getBBox из Raphael.js)
Control.prototype.box = function () {
  return this.renderControl.getNode(this.map).getBBox();
}

//Обработка действия контроллера (клик)
Control.prototype.action = function () {}

function GraphVisio(name, param) {
	GraphVisio.superclass.constructor.call(this, name, param);
	this.data = []
	
	//var axisLabels = this.getAxisLabelsSignature();
	
	this.scaleOfGraph = {
		xaxis: {
			min: 0,
			max: 30,
			step: 30,
			axisLabel: "Время(с)"//axisLabels.xaxisLabel
		},
		yaxis: {
			min: 0,
			max: 0,
			step: 0,
			axisLabel: ""//axisLabels.yaxisLabel
		}
	}
}
extend(GraphVisio, Logic);

//Установить параметры отрисовки графика (минимальное и максимальное значение и шаг масштабирования по осям)
GraphVisio.prototype.setScaleOfGraph = function(param) {
	this.scaleOfGraph.xaxis.min = param.xaxis.min;
	this.scaleOfGraph.xaxis.max = param.xaxis.max;
	this.scaleOfGraph.xaxis.step = param.xaxis.step;
	this.scaleOfGraph.xaxis.axisLabel = param.xaxis.axisLabel;
	
	this.scaleOfGraph.yaxis.min = param.yaxis.min;
	this.scaleOfGraph.yaxis.max = param.yaxis.max;
	this.scaleOfGraph.yaxis.step = param.yaxis.step;
	this.scaleOfGraph.yaxis.axisLabel = param.yaxis.axisLabel;
}

GraphVisio.prototype.setLabelOfXAxis = function(label) {
	this.scaleOfGraph.xaxis.axisLabel = label;
}

//Установить шаг масштабирования по Ox
GraphVisio.prototype.setStepOfXAxis = function(step) {
	if (this.scaleOfGraph.xaxis.step < step) {
		this.scaleOfGraph.xaxis.step = step;
	}	
}

//Установить шаг масштабирования по Oy
GraphVisio.prototype.setStepOfYAxis = function(step) {
	if (this.scaleOfGraph.yaxis.step < step) {
		this.scaleOfGraph.yaxis.step = step;
	}
}

//Установить шаги масштабирования по осям графика
GraphVisio.prototype.setStepsOfAxis = function(param) {
	this.setStepOfXAxis(param.xaxis.step);//this.scaleOfGraph.xaxis.step += param.xaxis.step;
	this.setStepOfYAxis(param.yaxis.step);//this.scaleOfGraph.yaxis.step += param.yaxis.step;
}

//Установить минимальное значение по Ox
GraphVisio.prototype.setMinOfXAxis = function(min) {
	this.scaleOfGraph.xaxis.min = min;
}

//Установить максимальное значение по Ox
GraphVisio.prototype.setMaxOfXAxis = function(max) {
	this.scaleOfGraph.xaxis.max = max;
}

//Установить минимальное значение по Oy
GraphVisio.prototype.setMinOfYAxis = function(min) {
	this.scaleOfGraph.yaxis.min = min;
}

//Установить максимальное значение по Oy
GraphVisio.prototype.setMaxOfYAxis = function(max) {
	this.scaleOfGraph.yaxis.max = max;
}

//Установить значения по Oy
GraphVisio.prototype.setYAxisAndRedraw = function(min, max, step) {
	if (min == "NaN" || max == "NaN" || step == "NaN") return;
	
	this.setMinOfYAxis(min);
	this.setMaxOfYAxis(max);
	this.setStepOfYAxis(step);
	
	var series = this.plot.getData();
	this.setPlot(series);
}

GraphVisio.prototype.zoomInScaleOfXAxisMin = function(step) {
	this.scaleOfGraph.xaxis.min -= step;//this.scaleOfGraph.xaxis.step;
}

GraphVisio.prototype.zoomInScaleOfXAxisMax = function(step) {
	this.scaleOfGraph.xaxis.max += step;//this.scaleOfGraph.xaxis.step;
}

GraphVisio.prototype.zoomInScaleOfYAxisMin = function(step) {
	this.scaleOfGraph.yaxis.min -= step;//this.scaleOfGraph.xaxis.step;
}

GraphVisio.prototype.zoomInScaleOfYAxisMax = function(step) {
	this.scaleOfGraph.yaxis.max += step;//this.scaleOfGraph.xaxis.step;
}

GraphVisio.prototype.zoomOutScaleOfXAxisMin = function(step) {
	this.scaleOfGraph.xaxis.min += step;//this.scaleOfGraph.xaxis.step;
}

GraphVisio.prototype.zoomOutScaleOfXAxisMax = function(step) {
	this.scaleOfGraph.xaxis.max -= step;//this.scaleOfGraph.xaxis.step;
}

GraphVisio.prototype.zoomOutScaleOfYAxisMin = function(step) {
	this.scaleOfGraph.yaxis.min += step;//this.scaleOfGraph.xaxis.step;
}

GraphVisio.prototype.zoomOutScaleOfYAxisMax = function(step) {
	this.scaleOfGraph.yaxis.max -= step;//this.scaleOfGraph.xaxis.step;
}

//Установить масштаб по Ox
GraphVisio.prototype.setScaleOfXAxis = function(param) {
	this.setMinOfXAxis(param.xaxis.min);//this.scaleOfGraph.xaxis.min = param.xaxis.min;
	this.setMaxOfXAxis(param.xaxis.max);//this.scaleOfGraph.xaxis.max = param.xaxis.max;
}

//Установить масштаб по Oy
GraphVisio.prototype.setScaleOfYAxis = function(param) {
	this.setMinOfYAxis(param.yaxis.min);//this.scaleOfGraph.yaxis.min = param.yaxis.min;
	this.setMaxOfYAxis(param.yaxis.max);//this.scaleOfGraph.yaxis.max = param.yaxis.max;
}

//Увеличить масштаб по Ox
GraphVisio.prototype.zoomInScaleOfXAxis = function(step) {
	this.zoomInScaleOfXAxisMin(step);
	this.zoomInScaleOfXAxisMax(step);
}

//Увеличить масштаб по Oy
GraphVisio.prototype.zoomInScaleOfYAxis = function(step) {
	this.zoomInScaleOfYAxisMin(step);
	this.zoomInScaleOfYAxisMax(step);
}

//Уменьшить масштаб по Ox
GraphVisio.prototype.zoomOutScaleOfXAxis = function(step) {
	this.zoomOutScaleOfXAxisMin(step);
	this.zoomOutScaleOfXAxisMax(step);
}

//Уменьшить масштаб по Oy
GraphVisio.prototype.zoomOutScaleOfYAxis = function(step) {
	this.zoomOutScaleOfYAxisMin(step);
	this.zoomOutScaleOfYAxisMax(step);
}

//Увеличить масштаб отрисовки графика
GraphVisio.prototype.zoomInScaleOfGraph = function(param) {
	//this.scaleOfGraph.xaxis.max += this.scaleOfGraph.xaxis.step;
	//this.scaleOfGraph.yaxis.max += this.scaleOfGraph.yaxis.step;
	this.zoomInScaleOfXAxis(this.scaleOfGraph.xaxis.step);
	this.zoomInScaleOfYAxis(this.scaleOfGraph.yaxis.step);
}

//Уменьшить масштаб отрисовки графика
GraphVisio.prototype.zoomOutScaleOfGraph = function(param) {
	//this.scaleOfGraph.xaxis.max -= this.scaleOfGraph.xaxis.step;
	//this.scaleOfGraph.yaxis.max -= this.scaleOfGraph.yaxis.step;
	this.zoomOutScaleOfXAxis(this.scaleOfGraph.xaxis.step);
	this.zoomOutScaleOfYAxis(this.scaleOfGraph.yaxis.step);
}

//Установить параметры области
GraphVisio.prototype.setPlot = function(series) {
	//var tichFormat = this.transferSystem(cnt90.state.graphic.timeBetweenMeasurements)[0];
	this.plot = $.plot(this.container, series, {
		grid: {	borderWidth: 1,	minBorderMargin: 20,labelMargin: 10,
			backgroundColor: { 	colors: ["#E6E6FA", "#E6E6FA"]	},
			margin: { top: 8, bottom: 20, left: 20},
		},
		xaxis: {
			min: this.scaleOfGraph.xaxis.min,
			max: this.scaleOfGraph.xaxis.max, 
			axisLabel: this.scaleOfGraph.xaxis.axisLabel, 
			tickFormatter: function (val, axis) {
								var tickParams = cnt90.control('graph').transferSystem(cnt90.state.graphic.timeBetweenMeasurements * val);
								//tickParams[0] = tickParams[0].toPrecision(3);//toFixed(2);// = [Math.floor(tickParams[0]), tickParams[1]];
								cnt90.control('graph').setLabelOfXAxis("Время (с)");//("Время (" + tickParams[1] + "с)");
								return tickParams + "с";//[0];
							},
		},
		yaxis: {
			min: this.scaleOfGraph.yaxis.min,
			max: this.scaleOfGraph.yaxis.max,
			axisLabel: this.scaleOfGraph.yaxis.axisLabel,
			tickFormatter: function (val, axis) {
								var tickParams = cnt90.control('graph').transferSystem(val);
								//tickParams[0] = tickParams[0].toPrecision(3);//toFixed(2);// = [Math.floor(tickParams[0]), tickParams[1]];
								//cnt90.control('graph').setLabelOfXAxis("Время (с)");//("Время (" + tickParams[1] + "с)");
								var myRe = /\s\W(\S+)\W/;
								var myArray = axis.options.axisLabel.match(myRe);
								return tickParams + (myArray == null ? "" : myArray[1]);//[0];
							},
		}/*,
		legend: {
			show: true
		}*/
	});
}

GraphVisio.prototype.getAxisLabelsSignature = function() {
	var axisLabels = {xaxisLabel: this.scaleOfGraph.xaxis.axisLabel, yaxisLabel: ""};
	
	var selected_item = cnt90.state.graphic.curInput;
	switch (selected_item) {
		case "meas_func_freq_input_a":
			axisLabels = this.getAxisLabelsSignatureInAccordanceWithTypeSignal(cnt90.state.signalA.typeSignal);
			break;
		case "meas_func_freq_input_b":
			axisLabels = this.getAxisLabelsSignatureInAccordanceWithTypeSignal(cnt90.state.signalB.typeSignal);
			break;
		case "meas_func_freq_input_c":
			axisLabels = this.getAxisLabelsSignatureInAccordanceWithTypeSignal(cnt90.state.signalC.typeSignal);
			break;
		case "meas_func_volt_input_a":
			axisLabels = this.getAxisLabelsSignatureInAccordanceWithTypeSignal(cnt90.state.signalA.typeSignal);
			break;
		case "meas_func_volt_input_b":
			axisLabels = this.getAxisLabelsSignatureInAccordanceWithTypeSignal(cnt90.state.signalB.typeSignal);
			break;
		case "meas_func_volt_input_c":
			axisLabels = this.getAxisLabelsSignatureInAccordanceWithTypeSignal(cnt90.state.signalC.typeSignal);
			break;
		case "A/B":
			//axisLabels.xaxisLabel = "Время (" + this.transferSystem(cnt90.state.graphic.timeBetweenMeasurements)[1] + "с)";
			axisLabels.yaxisLabel = "Отношение";
			break;
		case "B/A":
			//axisLabels.xaxisLabel = "Время (" + this.transferSystem(cnt90.state.graphic.timeBetweenMeasurements)[1] + "с)";
			axisLabels.yaxisLabel = "Отношение";
			break;
		case "C/A":
			//axisLabels.xaxisLabel = "Время (" + this.transferSystem(cnt90.state.graphic.timeBetweenMeasurements)[1] + "с)";
			axisLabels.yaxisLabel = "Отношение";
			break;
		case "С/B":
			//axisLabels.xaxisLabel = "Время (" + this.transferSystem(cnt90.state.graphic.timeBetweenMeasurements)[1] + "с)";
			axisLabels.yaxisLabel = "Отношение";
			break;
		case "IntervalAtoB":
            axisLabels.yaxisLabel = "Интервал (с)";
            break;
        case "IntervalBtoA":
            axisLabels.yaxisLabel = "Интервал (с)";
            break;
        case "rise_input_a":
            axisLabels.yaxisLabel = "Длительность (с)";
            break;
        case "fall_input_a":
            axisLabels.yaxisLabel = "Длительность (с)";
            break;
        case "rise_input_b":
            axisLabels.yaxisLabel = "Длительность (с)";
            break;
        case "fall_input_b":
            axisLabels.yaxisLabel = "Длительность (с)";
            break;
        case "pulse_width_a":
            axisLabels.yaxisLabel = "Длительность (с)";
            break;
        case "pulse_width_b":
            axisLabels.yaxisLabel = "Длительность (с)";
            break;
        case "duty_a":
            axisLabels.yaxisLabel = "Коэфф.заполнения";
            break;
        case "duty_b":
            axisLabels.yaxisLabel = "Коэфф.заполнения";
            break;
        case "A_and_B":
            axisLabels.yaxisLabel = "Фаза (°)";
            break;
        case "A_and_C":
            axisLabels.yaxisLabel = "Фаза (°)";
            break;
        case "B_and_C":
            axisLabels.yaxisLabel = "Фаза (°)";
            break;
	};
	return axisLabels;
}

GraphVisio.prototype.transferSystem = function (value) {
	if ((value >= 0.000000000001 && value < 0.000000001) || (value <= -0.000000000001 && value > -0.000000001)) {
		return (value * 1000000000000).toFixed(1) + " п";
	} else if ((value >= 0.000000001 && value < 0.000001) || (value <= -0.000000001 && value > -0.000001)) {
        return (value * 1000000000).toFixed(1) + " н";
    } else if ((value >= 0.000001 && value < 0.001) || (value <= -0.000001 && value > -0.001)) {
        return (value * 1000000).toFixed(1) + " мк";
    } else if ((value >= 0.001 && value < 1) || (value <= -0.001 && value > -1)) {
        return (value * 1000).toFixed(1) + " м";
    } else if ((value >= 1 && value < 1000) || value == 0 || (value <= -1 && value > -1000)) {
        return (value).toFixed(1) + " ";
    } else if ((value >= 1000 && value < 1000000) || (value <= -1000 && value > -1000000)) {
        return (value / 1000).toFixed(1) + " к";
    } else if ((value >= 1000000 && value < 1000000000) || (value <= -1000000 && value > -1000000000)) {
        return (value / 1000000).toFixed(1) + " М";
    } else if (value >= 1000000000 || value <= -1000000000) {
        return (value / 1000000000).toFixed(1) + " Г";
    }
  return value.toFixed(1) + " ";
}

GraphVisio.prototype.getAxisLabelsSignatureInAccordanceWithTypeSignal = function(typeSignal) {
  var axisLabels = {xaxisLabel: this.scaleOfGraph.xaxis.axisLabel, yaxisLabel: ""};
  
  var current_pos_way = cnt90.generator.current_pos_way;
  if (this.map.menu_bar.current_pos_way != "menu_statplot_numerical") {
	current_pos_way = this.map.menu_bar.current_pos_way;
  }
  
  switch(typeSignal) {
		case "sin":
			//axisLabels.xaxisLabel = "Время (" + this.transferSystem(cnt90.state.graphic.timeBetweenMeasurements)[1] + "с)";
			if (current_pos_way == "menu_meas_func_freq_freq") {
				axisLabels.yaxisLabel = "Частота (Гц)";
				break;
			}
			if (current_pos_way == "menu_meas_func_volt") {
				axisLabels.yaxisLabel = "Напряжение (В)";
				break;
			}
			axisLabels.yaxisLabel = "";			
			break;
		case "impulse":
			//axisLabels.xaxisLabel = "Время (" + this.transferSystem(cnt90.state.graphic.timeBetweenMeasurements)[1] + "с)";
			if (current_pos_way == "menu_meas_func_freq_freq") {
				axisLabels.yaxisLabel = "Частота (Гц)";
				break;
			}
			if (current_pos_way == "menu_meas_func_volt") {
				axisLabels.yaxisLabel = "Напряжение (В)";
				break;
			}
			axisLabels.yaxisLabel = "";			
			break;
		case "frequencyModulation":
			//axisLabels.xaxisLabel = "Время (" + this.transferSystem(cnt90.state.graphic.timeBetweenMeasurements)[1] + "с)";
			if (current_pos_way == "menu_meas_func_freq_freq") {
				axisLabels.yaxisLabel = "Частота (Гц)";
				break;
			}
			if (current_pos_way == "menu_meas_func_volt") {
				axisLabels.yaxisLabel = "Напряжение (В)";
				break;
			}
			axisLabels.yaxisLabel = "";			
			break;
		case "amplitudeModulation":	
			//axisLabels.xaxisLabel = "Время (" + this.transferSystem(cnt90.state.graphic.timeBetweenMeasurements)[1] + "с)";
			if (current_pos_way == "menu_meas_func_freq_freq") {
				axisLabels.yaxisLabel = "Частота (Гц)";
				break;
			}
			if (current_pos_way == "menu_meas_func_volt") {
				axisLabels.yaxisLabel = "Напряжение (В)";
				break;
			}
			axisLabels.yaxisLabel = "";			
			break;
	};
	return axisLabels;
}

GraphVisio.prototype.clearPlot = function() {
	var axisLabels = this.getAxisLabelsSignature();
	
	
	this.scaleOfGraph = {
		xaxis: {
			min: 0,
			max: 30,
			step: 30,
			axisLabel: axisLabels.xaxisLabel
		},
		yaxis: {
			min: 0,
			max: 0,
			step: 0,
			axisLabel: axisLabels.yaxisLabel
		}
	}
	this.data = [];
	var series = [{data: [], lines: {fill: true	}}];
	this.setPlot(series);
}

GraphVisio.prototype.setMap = function (map) {
	GraphVisio.superclass.setMap.call(this, map);
	this.container = $("#placeholder");
	var series = [{data: [], lines: {fill: false	}}];

	this.setPlot(series);
	
	map.addListener('signal', this, function(source, signal) {
		//this.setLabelsOfXAxis();
		
		if (this.data.length >= this.scaleOfGraph.xaxis.max) {
			this.setStepOfXAxis(Math.abs(this.data.length));
			this.zoomInScaleOfXAxisMax(this.scaleOfGraph.xaxis.step);//this.zoomInScaleOfGraph();
			
			var series = this.plot.getData();
			this.setPlot(series);
		}
		
		if (cnt90.state.limits.state == "OFF" || cnt90.state.limits.state == "ALARM" || cnt90.state.limits.state == "ALARM_STOP") {
			if (this.data[this.data.length - 1] >= this.scaleOfGraph.yaxis.max) {
				this.setStepOfYAxis(Math.abs(this.data[this.data.length - 1]));
				this.zoomInScaleOfYAxisMax(this.scaleOfGraph.yaxis.step);//this.zoomInScaleOfGraph();
				
				
				var series = this.plot.getData();
				this.setPlot(series);
			}
			
			if (this.data[this.data.length - 1] <= this.scaleOfGraph.yaxis.min) {
				this.setStepOfYAxis(Math.abs(this.data[this.data.length - 1]));
				this.zoomInScaleOfYAxisMin(this.scaleOfGraph.yaxis.step);
				
				var series = this.plot.getData();
				this.setPlot(series);
			}
		}
		
		this.data.push(signal);
		
		var res = [];
		var last = 0;
		for (var i = 0; i < this.data.length; ++i) {
			var d = this.data[i];
			
			if (last != 0 && d == 0 && i > 0) 
				res.push([i, last]);
			if (last == 0 && d > 0 && i > 0) 
			
			res.push([i, last]);
			res.push([i, d])
			last = this.data[i];
		}
		
		var series = [{data: res, lines: {fill: false}, color: "#33CC66"}];
		this.setPlot(series);
		this.plot.setData(series);
		this.plot.draw();
	});
}
function extend(Child, Parent) {
  var F = function() { }
  F.prototype = Parent.prototype
  Child.prototype = new F()
  Child.prototype.constructor = Child
  Child.superclass = Parent.prototype
}

// вспомогательная функция. распечатка в стиле print_r. Троицкий
//// Функия переписана. Теперь свойства сортируются, и если объекты отличаются только порядком свойств, то они считаются равными 
function print_object(obj, condition) {
    var s = "";
    var foo = "";
    var arr = [], i;
    // записываем свойства в массив, затем его сортируем
    for (i in obj) {
        if (obj.hasOwnProperty(i)) {
            arr.push(i);
        }
    }
    arr.sort();
    for (i = 0; i < arr.length; i++) {
        if (typeof obj[arr[i]] == "function")
            continue;
        if (!(condition === undefined))
            if (condition(arr[i]))
                continue;
        if (typeof obj[arr[i]] == "object")
            foo = print_object(obj[arr[i]], condition);
        else
            foo = obj[arr[i]];
        s += arr[i] + ": " + foo + ";\n";
    }
    return s;
}

function Add_CSS(css_file) {
  $("head").append("<link>");
  css = $("head").children(":last");
  css.attr({
    rel:  "stylesheet",
    type: "text/css",
    href: css_file
  });
}

// Новый режим Обучение и контроль. Троицкий
var deviceMode = {EMULATOR: 0, HELP: 1, TRAINING: 2, CONTROL: 3}

function cp(o) {
  return jQuery.extend(true, {}, o);
}

function log10(val) {
  return Math.log(val) / Math.LN10;
}
